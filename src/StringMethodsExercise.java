import java.util.Arrays;

public class StringMethodsExercise {
    public static void main(String[] args) {

        String  myLength = "Good afternoon, good evening, and good night";
        System.out.println(myLength.length());

        String  uCase = "Good afternoon, good evening, and good night";
        System.out.println(uCase.toUpperCase());

        String  lCase = "Good afternoon, good evening, and good night";
        System.out.println(lCase.toLowerCase());

/*
Use the some string, but instead of using 'int myLength', use 'String uCase'
	Write some java code that will display the entire string in all uppercase
Use the same code but instead of using the toUpperCase() method, use the toLowerCase() method.
	Print it out in the console, what do you see?
	good afternoon, good evening, and good night
 */

//        Copy this code into your main method
        String firstSubstring = "Hello World".substring(10);
//        Print it out in the console, what do you see?
//        World
//        Change the argument to 3, print out in the console. What do you get?
//        lo World
//        Change the argument to 10, print it out in the console. What do you get?
//        d

        System.out.println(firstSubstring);


//Copy this code into your main method
	String message = "Good evening, how are you?";
//	Using the substring() method, make your variable print out "Good evening"
//	Now using the substring() method, try to make your variable print out "how are you?"
        System.out.println(message.substring(0,12));
        System.out.println(message.substring(14));


/*
Create a char variable named 'myChar' and assign the value "San Antonio"
	Using the charAt() method return the capital 'S' only.
	Change the argument in the charAt() method to where it returns the capital 'A' only.
	Change the argument in the charAt() method to where it returns the FIRST lowercase 'o' only.
 */

        String myChar = "San Antonio";
        System.out.println(myChar.charAt(0));
        System.out.println(myChar.charAt(4));
        System.out.println(myChar.charAt(7));

        /*
        Create a String variable name 'alpha' and assign all the names in the cohort in ONE string.
	Using the split() method, create another variable named 'splitAlpha' where it displays the list of names separated by a comma.
	Print out your results in the console
	Ex. [Peter, John, Andy, David];
         */

        String charlie = "Josh Jose Nick";
        String[] splitCharlie = charlie.split(" ");
        System.out.println(Arrays.toString(splitCharlie));

        /*
        Use the following string variables.
    String m1 = "Hello, ";
	String m2 = "how are you?";
	String m3 = "I love Java!"
	Using concatenation, print out all three string variables that makes a complete sentence.

         */

        String m1 = "Hello, ";
        String m2 = "how are you?";
        String m3 = "I love Java!";
        System.out.println(m1 + m2 + m3);

/*
Use the following integer variable
	int result = 89;
	Using concatenation, print out "You scored 89 marks for your test!"
	*89 should not be typed in the sout, it should be the integer variable name.
 */

        int result = 89;
        System.out.printf("You scored %s marks for your test!", result);;
    }
}
