package AbstractInterfaceDemo;

public class Main {
    public static void main(String[] args){
        MyClass classMy = new MyClass();
        classMy.someMethod();
        System.out.println(classMy.myInt);
        classMy.someDefaultMethod();
        //intellij suggestion for error
        MyInterface.someStaticMethod();
    }
}
