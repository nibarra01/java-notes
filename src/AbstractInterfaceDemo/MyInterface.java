package AbstractInterfaceDemo;

public interface MyInterface {
    public int myInt = 5;
    public static void someStaticMethod(){
        System.out.println("This is a static method in my interface");
    }
    public default void someDefaultMethod(){
        System.out.println("This is a default method in my interface");
    }
}
