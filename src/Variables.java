public class Variables {
    public static void main(String[] args) {
        int favoriteNum = 999999;
        System.out.println(favoriteNum);

        String myName = "Nicholas";
        System.out.println(myName);
//        Change your code to assign a character value to myName. What do you notice?
//        java: incompatible types: java.lang.String cannot be converted to char
//        Change your code to assign the value 3.14159 to myName. What happens?
//        java: incompatible types: double cannot be converted to java.lang.String

//        float myNum = 3.14;
//        System.out.println(myNum);
//        Declare an long variable named myNum, but do not assign anything to it. Next try to print out myNum to the console. What happens?
//        java: variable myNum might not have been initialized
//        Change your code to assign the value 3.14 to myNum. What do you notice?
//        java: incompatible types: possible lossy conversion from double to long

//        Change your code to assign the value 123L (Note the 'L' at the end) to myNum.
//Change your code to assign the value 123 to myNum.

//        Why does assigning the value 3.14 to a variable declared as a long not compile, but assigning an integer value does?
//        only float and double hold decimals

//        Change your code to declare myNum as a float. Assign the value 3.14 to it. What happens? What are two ways we could fix this?
//        java: incompatible types: possible lossy conversion from double to float
//        double myNum = 3.14
//        float myNum = (float) myNum
//        float myNum = 3.14f

//        int x = 10;
//        System.out.println(x++);
//        System.out.println(x);

//        int x = 10;
//        System.out.println(++x);
//        System.out.println(x);

//        What is the difference between the above code blocks? Explain why the code outputs what it does.
//Try to create a variable named class. What happens?
//        the placement of the '++'
//        the first one displays the variable then adds 1 to the variable, the second increments the variable by one then displays it

//        Java doesn't allow class to be used as a name and instead returns errors as if the data type and class were supposed to be on different lines and are incomplete declarations

//        Object is the most generic type in Java. You can assign any value to a variable of type Object. What do you think will happen when the following code is run?
//String theNumberEight = "eight";
//Object o = theNumberEight;
//int eight = (int) o;

//        either int eight = (int) o works like eight = parseInt(o) or it's going to return an error because it doesn't convert word numbers in to number numbers

//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o;

//        Copy and paste the code above and then run it. Does the result match with your expectation?
//How is the above example different from this code block?
//        int eight = (int) "eight";

//        2 less lines, no string or object involved
//        int eight = (int) "eight";

//        int x = 5;
//x = x + 6;
//        x += 6;

//int x = 7;
//int y = 8;
//y = y * x;
//        y *= x

//int x = 20;
//int y = 2;
//x = x / y;
//y = y - x;
//        x /= y
//        y -= x

//        What happens if you assign a value to a numerical variable that is larger (or smaller) than the type can hold? What happens if you increment a numeric variable past the type's capacity?
//Hint: Integer.MAX_VALUE is a class constant (we'll learn more about these later) that holds the maximum value for the int type.
//        int i = Integer.MAX_VALUE + 1;
//        System.out.println(i);

    }
}
