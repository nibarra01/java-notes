public class MethodLesson {

    public static void main(String[] args) {
//        System.out.println(greeting("Charlie"));

        //calling sayHi() and passing in values for the parameters
//        sayHi("Hey there", "Josh", 1000000);

//       call our returnFive()
//        System.out.println();
//        System.out.println(returnFive());


//        System.out.println(yelling("I'm not yelling"));

//        sayHello(3);
//        /*
//        World Hello
//        World Hello
//        World Hello
//         */
//        sayHello("Charlie");
//        // Charile Hello
//        sayHello("Jose", "Hey");
//        // Hey Jose




        String changeMe = "Hello CodeBound";
//        call our changeThis()
        changeThis(changeMe);
        System.out.println(changeMe);

//         recursion
        int result = addNums(10);
        System.out.println(result);


    }

    //syntax of defining a method
//    public static returnType methodName(param1, param2, ... paramn){
//        the statement(s) of the method
//                a return statement
//    }


    public static String greeting(String name){
        return String.format("Hello there, %s", name);
    }

//    public - the visibility modifier
//    static - the presence of this keyword defines that the method belongs to the class as opposed to instances of it.
//    String - the return type of the method
//    greeting - the name of the method

    public static void sayHi(String greeting, String name, int salary){
        System.out.printf("%s %s!\nYou make %s", greeting, name, salary);
    }

    public static int returnFive(){
        System.out.println("This is calling our returnFive()");
        System.out.println(5);
        return 5;
    }

    public static String yelling(String s){
//        returnFive();
        System.out.println(returnFive());
        return s.toUpperCase() + "!";
    }

//    method overloading
//    defining multiple methods with the same name but with different parameter types, parameter order or number of parameters

//    v1
    public static void sayHello(int num){
        for (int i = 0; i < num; i++){
            sayHello();
        }
    }

//    v2
    public static void  sayHello(){
        sayHello("Hello", "World");
    }

//    v3
    public static void  sayHello(String name){
        sayHello("hello", name);
    }

//    v4
    public static void sayHello(String name, String greeting){
        System.out.println(greeting + " " + name);
    }


//    passing parameters to methods
    public static void changeThis(String sentence){
        sentence = "Denim Denim denim";
    }

//    Recursion
//    the technique of making a method call itself

//    use recursion to add all of the numbers up to 10
    public static int addNums(int num){
        if (num > 0){
            return num + addNums(num - 1);
        }
        else {
            return 0;
        }

    }


}
