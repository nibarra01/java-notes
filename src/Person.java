public class Person {
    private String name;

    public Person(){}
    public Person(String name){
        this.name = name;
    }


    public String getName(){
        return this.name;
    }
//- returns the person's name
    public void setName(String name){
        this.name = name;
    }
//- // changes the name property to the passed value
    public void sayHello(){
        System.out.println("Hello " + this.name);
    }
//- // prints a message to the console using the person's name


}
