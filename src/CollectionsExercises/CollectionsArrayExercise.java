package CollectionsExercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CollectionsArrayExercise {
    public static void main(String[] args) {
//        Create a new class called CollectionsArraysExercise
//                Create a program that will print out an array of integers.
        One(1,2,3,4,5,6);
//        Create a program that will print out an array of strings
        Two("an", "array", "of", "strings");
//        Create a program that iterates through all elements in an array. The array should hold the names of everyone in Charlie Class
        Three();
//        Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
        Four();
//                Create a program to remove the third element from an array list. You can create a new array or use a previous one.
        Five("Mobius One", "Yellow 13", "Yellow 4", "Omega 11");
//                Create an array of Dog breeds. Create a program that will sort the array list.
        Six();
//                Create an array of cat breeds. Create a program that will search an element in the array List.
        Seven();
//        Now create a program that will reverse the elements in the array.
        String[] sus = {"Red", "Blue", "Yellow", "Orange", "Green", "Lime", "Cyan", "White", "Black", "Brown", "Purple"};
        System.out.println();
        System.out.println(Arrays.toString(sus));
        sus = Eight(sus);
        System.out.println(Arrays.toString(sus));

    }
    public static void One(int a, int b, int c, int d, int e, int f){
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(a,b,c,d,e,f));
        System.out.println(list);
    }

    public static void Two(String w, String a, String s, String d){
        ArrayList<String> too = new ArrayList<>();
        too.add(w);
        too.add(a);
        too.add(s);
        too.add(d);
        System.out.println(too);
    }

    public static void Three(){
        ArrayList<String> charlieCohort = new ArrayList<>();
        charlieCohort.add("Jose");
        charlieCohort.add("Josh");
        charlieCohort.add("Nick");
        charlieCohort.add("Stephen");
        charlieCohort.add("Karen");

        for(String x : charlieCohort){
            if (charlieCohort.indexOf(x) <= 2){
                System.out.println(x + " - Student");
            } else {
                System.out.println(x + " - Instructor");
            }
        }
    }
    public static void Four(){
        ArrayList<String> UC = new ArrayList<>(Arrays.asList(
                "Mobile Suit Gundam",
                "Mobile Suit Zeta Gundam",
                "Mobile Suit Gundam ZZ",
                "Mobile Suit Gumdam: The 08th MS Team",
                "Mobile Suit Guddam 0080: War in the Pocket",
                "Mobile Suit Gundam: Stardust Memory"
        ));
        System.out.println("\n"+UC);

        UC.add("Mobile Suit Gundam: Char's Counterattack");

        UC.add(0,"Mobile Suit Gundam THE ORIGIN");
        System.out.println(UC+"\n");
    }

    public static void Five(String one, String two, String three, String four){
        ArrayList<String> ac4 = new ArrayList<>(Arrays.asList(one, two, three, four));
        System.out.println("\n"+ac4);
        ac4.remove(2);
        System.out.println(ac4+"\n");
    }

    public static void  Six(){
        ArrayList<String> dog = new ArrayList<>();
        dog.add("Golden Retriever");
        dog.add("Husky");
        dog.add("Doberman");
        dog.add("Dalmatian");
        dog.add("Corgi");
        dog.add("Shiba Inu");

        System.out.println();
        System.out.println(dog);
        Collections.sort(dog);
        System.out.println(dog);
    }

    public static void Seven(){
        System.out.println();
        ArrayList<String> cat = new ArrayList<>();
        cat.add("Persian");
        cat.add("Maine Coon");
        cat.add("Siamese");
        cat.add("Exotic");
        cat.add("Abyssinian");
        cat.add("Oriental");
        cat.add("Birman");
        cat.add("American Shorthair");
        cat.add("Scottish Fold");
        cat.add("Burmese");
        System.out.println(cat);
        System.out.println("Index of \"American Shorthair\": " + cat.indexOf("American Shorthair"));
    }

    public static String[] Eight(String[] emergencyMeeting){
        /* initlally typed
         for (String crewmate : emergencyMeeting){
            tasksInElectrical.add(crewmate);
        }
         */
        /* suggested to change to this
                tasksInElectrical.addAll(Arrays.asList(emergencyMeeting));

         */
        //then that suggestion was suggested to change to this
        ArrayList<String> tasksInElectrical = new ArrayList<>(Arrays.asList(emergencyMeeting));
        Collections.reverse(tasksInElectrical);
        String[] wasNotTheImposter = new String[11];
        int vote = 0;
        for (String crewmate : tasksInElectrical){
            wasNotTheImposter[vote] = crewmate;
            vote++;
        }
        return wasNotTheImposter;
    }


}
