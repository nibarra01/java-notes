package CollectionsExercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {
        HashMap<String, String> ZeonOYW = new HashMap<String, String>();
        ZeonOYW.put("MS-05","Zaku I");
        ZeonOYW.put("MS-06", "Zaku II");
        ZeonOYW.put("MS-07", "Gouf");
        ZeonOYW.put("MS-09", "Dom");
        ZeonOYW.put("MS-14", "Gelgoog");
        System.out.println(ZeonOYW);
//        Create a program that will append a specified element to the end of a hash map.
        ZeonOYW = One(ZeonOYW);
        System.out.println(ZeonOYW);
//        Create a program that will iterate through all the elements in a hash map
        Two(ZeonOYW);
//        Create a program to test if a hash map is empty or not.
        System.out.println("\nSystem.out.println(ZeonOYW.isEmpty());");
        System.out.println(ZeonOYW.isEmpty());
//                Create a program to get the value of a specified key in a map
        System.out.println("\nSystem.out.println(ZeonOYW.get(\"MS-06\"));");
        System.out.println(ZeonOYW.get("MS-06"));
//        Create a program to test if a map contains a mapping for the specified key.
        System.out.println("\nSystem.out.println(ZeonOYW.containsKey(\"RGM-079\"));");
        System.out.println(ZeonOYW.containsKey("RGM-079"));
//                Create a program to test if a map contains a mapping for the specified value.
        System.out.println("\nSystem.out.println(ZeonOYW.containsValue(\"Gouf\"));");
        System.out.println(ZeonOYW.containsValue("Gouf"));
//                Create a program to get a set view of the keys in a map
        System.out.println("\nSystem.out.println(ZeonOYW.entrySet());");
        System.out.println(ZeonOYW.entrySet());
//        Create a program to get a collection view of the VALUES contained in a map.
        System.out.println("\nSystem.out.println(ZeonOYW.values());");
        System.out.println(ZeonOYW.values());
    }
    public static HashMap<String, String> One(HashMap<String, String> zeonic){
        zeonic.put("MSM-03", "Gogg");
        zeonic.put("MSM-04", "Acguy");
        zeonic.put("MSM-07", "Z'Gok");
        zeonic.put("MSM-10", "Zock");

        return zeonic;
    }

    public static void Two(HashMap<String, String> ms){
        for (Map.Entry<String, String> x : ms.entrySet()){
            System.out.println("MS name: " + x.getValue() + "\t Model number: " +  x.getKey());
        }
    }
}
