package Collections;
/*
collections
- a data structure that can eb used to group, or colelct, objects
The Java standard library, a collection of code that comes with the Java language,
contains many collections to help with common programming tasks

main focus:
ArrayList
HashMaps

ArrayList
- represents an array that can change its size
- all elements in an array must be objects and same data types
- use ArrayList as a "wrapper" around the native Java array

- used a little different; they have methods that handle common array operations,


.size() - returns the number of elements in the array
.add() - add an element to the collection (optionally) at the specified index
.get() - return the element at the specified index
.indexOf() - return the first found index of the given item, or -1 if not found
.contain() - checks if a collection contains a given element
.lastIndexOf() - find the last index of the given element, if not found will return -1
.isEmpty() - checks if hte list is empty
.remove() - removes the first occurrence of an item

syntax:
ArrayList<type> name - new ArrayList<>();

HashMap
- data structure for key-value pairs
- implemented by using the HashMap class in Java
- very similar ot objects in javascript
- all of the keys in the hash map must be the same type and all values must be the same type
- However they keys and values do not necessarily have to be the same type


.put() - set a key-pair value
.get() - return the value associated with the given key, if not returns null
.getOrDefault() - like the .get but with a defined value instead of null
.containsKey() - check if a key exists in the map
.containsValue() - check if a value exists on the map
.putIfAbsent() only set a key-value pair if it does not exists.
    .remove() remove a key-value pair from the map.
    .replace() replace a value at a given key.
    .clear() empty the map.
    .isEmpty() check if the map is empty.

    **Like ArrayLists we must define the type of the keys and values when the hash map is created.
    **HashMap also have a string representation.

    Syntax :
    HashMap<Type, Type> nameOfVariable = new HashMap<>();


 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        //ArrayList Examples

        ArrayList<Integer> numbers = new ArrayList<>();

//        add values to our ArrayList
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(25);

//        System.out.println(numbers);

        ArrayList<String> names = new ArrayList<>(Arrays.asList(

                "Pop",
                "Duncan",
                "Parker",
                "Ginobili"
        ));
        names.add("Robinson");
//        System.out.println(names);

        ArrayList<Integer> myNumbers = new ArrayList<>();
        myNumbers.add(23);
        myNumbers.add(50);
        myNumbers.add(32);
        myNumbers.add(91);

//        peeking into the list
        int num = myNumbers.get(0);
        int myNum = myNumbers.get(2);

//        System.out.println(num);
//        System.out.println(myNum);
//
////        edit elements
//        myNumbers.set(3,100);
//        System.out.println(myNumbers);
//
////        remove elements
//        myNumbers.remove(2);
//        System.out.println(myNumbers);
//
////        reordering list
//        Collections.sort(myNumbers);
//        Collections.reverse(myNumbers);
//        System.out.println(myNumbers);



//        ArrayList<String> roasts = new ArrayList<>(Arrays.asList("light","medium", "bold", "dark"));
//        System.out.println(roasts.contains("espresso"));
//        System.out.println(roasts.lastIndexOf("bold"));
//        System.out.println(roasts.isEmpty());
//        System.out.println(roasts.indexOf("medium"));

// HASHMAP

        HashMap<String, String> usernames = new HashMap<>();
        usernames.put("Karen", "krivas123");
        usernames.put("Stephen", "sguedea001");
        usernames.put("Juan", "thejuanandonly210");
        usernames.put("Josh", "sleepy512");

        //getting values from and info about hash map

        System.out.println(usernames);
        System.out.println(usernames.get("Juan"));
        System.out.println(usernames.get("Stephen"));

//        updating hash map
//        usernames.put("Jose", "jose321");
//        System.out.println(usernames);


        usernames.put("Jose", "jose213");
        System.out.println(usernames);

//        replacing in our hash map
        usernames.replace("Stephen", "goat247");

//        remove pairs from hash map
        System.out.println(usernames.remove("Karen")); // krivas123
        System.out.println(usernames); // updates hash map that does not contian krivas123

        usernames.clear();
        System.out.println(usernames);
        System.out.println(usernames.isEmpty());
    }
}
