package ExceptionsAndErrorHandling;
/*
Exception handling/error handling
- allows us to control the flow of a program when an error occurs
- there are different types of exceptions, when and how to use them, and how to create custom exceptions.

An exception is an unexpected event that occurs during program execution.
- It affects the flow of the program instructions which can cause the program to terminate abnormally.

An exception can occur for many reasons.  some of the are
- invalid user input
- device failure
- loss of network connection
- physical limitations (out of disk emmory)
-code errors
- opening an unavailable file

The exception hierarchy also has two branches: RuntimeException and IOException.
RuntimeException
- A runtime exception happens due to a programming error. They are also known as unchecked exceptions.

These exceptions are not checked at compile-time but run-time.

Some of the common runtime exceptions are:

- Improper use of an API - IllegalArgumentException
- Null pointer access (missing the initialization of a variable) - NullPointerException
- Out-of-bounds array access - ArrayIndexOutOfBoundsException
- Dividing a number by 0 - ArithmeticException

You can think about it in this way. “If it is a runtime exception, it is your fault”.

The NullPointerException would not have occurred if you had checked whether the variable
was initialized or not before using it.

An ArrayIndexOutOfBoundsException would not have occurred if you tested the array index against the array bounds.

IOException
- also known as a checked exception.
They are checked by the compiler at the compile-time and the programmer
 is prompted to handle these exceptions.


Some of the examples of checked exceptions are:

- Trying to open a file that doesn't exist results in FileNotFoundException
- Trying to read past the end of a file

In Java, we use the exception handler components try, catch and finally blocks to handle exceptions.

Syntax for try-catch-finally

try {
        do something
    }
catch (type of error) {
        do something else when an error occurs
    }
finally {
    do this regardless of whether the try or catch condition is met
    }

 */

public class ExceptionsLesson {
    public static void main(String[] args) {
        // Example try-catch block
//        try{
//            int divideByZero = 5/0;
//            System.out.println("Rest of the code in try block");
//        }catch (ArithmeticException bad){
//            System.out.println("Arethmetic Exception: " + bad.getMessage()); //display default exception message for ArithmeticException
//        }

//        example multiple catch blcoks
//

//        try-catch-finally
        try {
            int divideByZero = 5/0;
        }
        catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("finally block is always executed");
        }
    }
}
