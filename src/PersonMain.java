public class PersonMain {
    public static void main(String[] args) {
        Person p1 = new Person("test");

        System.out.println(p1.getName());

        Person p2 = new Person();
        p2.setName("TEST");
        p2.sayHello();


//        Person person1 = new Person("John");
//        Person person2 = new Person("John");
//        System.out.println(person1.getName().equals(person2.getName())); //returns true
//        System.out.println(person1 == person2); // expected true returns false

//        Person person1 = new Person("John");
//        Person person2 = person1;
//        System.out.println(person1 == person2); //warning says always returns true

//        Person person1 = new Person("John");
//        Person person2 = person1;
//        System.out.println(person1.getName()); //John
//        System.out.println(person2.getName()); //John
//        person2.setName("Jane");
//        System.out.println(person1.getName()); //expected John returns Jane
//        System.out.println(person2.getName()); //Jane
    }
}
