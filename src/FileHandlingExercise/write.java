package FileHandlingExercise;
import java.io.*;

public class write {
    public static void main(String[] args) {

        //overwriting a text file/file's text

//        String overwriteText = "test";
//        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./Albums/test.txt"))) {
//            rewrite.write(overwriteText);
//            rewrite.newLine();
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//        }

        writeAlbumOne();
        writeAlbumTwo();
        writeAlbumThree();
        System.out.println("Write complete.");
    }

    private static void writeAlbumOne(){
        String overwriteText = "Artist: Porter Robinson\nYear: 2014\nGenre: Synth-pop, chillwave, dream pop, electropop, ambient";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./Albums/Worlds.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeAlbumTwo(){
        String overwriteText = "Artist: Anamanaguchi\nYear: 2013\nGenre: Chiptune, synthpop, electropop, pop punk, nintendocore, bitpop";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./Albums/Endless_Fantasy.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeAlbumThree(){
        String overwriteText = "Artist: Snail's House\nYear: 2017\nGenre: \tKawaii future bass, orchestral";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./Albums/Pixel_Galaxy.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
