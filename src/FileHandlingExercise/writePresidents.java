package FileHandlingExercise;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class writePresidents {
    private static void writePresidentOne(){
        String overwriteText = "JAMES KNOX POLK\n11th President of the United States\nDemocratic Party\n1845-1849\n\"No President who performs his duty faithfully and conscientiously can have any leisure. I prefer to supervise the whole operations of the government myself rather than intrust the public business to subordinates, and this makes my duties very great.\"";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./data/JamesKPolk.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void writePresidentTwo(){
        String overwriteText = "THEODORE ROOSEVELT\n26th President of the United States\nRepublican Party\n1901-1909\n\"Ladies and gentlemen, I don't know whether you fully understand that I have just been shot, but it takes more than that to kill a Bull Moose.\"";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./data/TheodoreRoosevelt.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void writePresidentThree(){
        String overwriteText = "FRANKLIN DELANO ROOSEVELT\n32nd President of the United States\nDemocratic Party\n1933-1945\n\"Yesterday, December 7, 1941 — a date which will live in infamy — the United States of America was suddenly and deliberately attacked by naval and air forces of the Empire of Japan.\"";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./data/FDR.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void writePresidentFour(){
        String overwriteText = "CALVIN COOLLDGE\n30th President of the United States\nRepublican Party\n1923-1929\n\"The chief business of the American people is business.\" ";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./data/SilentCal.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void writePresidentFive(){
        String overwriteText = "THOMAS JEFFERSON\n3rd President of the United States\nDemocratic-Republican Party\n1801-1809\n" +
                "1. Never put off till to-morrow what you can do to-day.\n" +
                "2. Never trouble another for what you can do yourself.\n" +
                "3. Never spend your money before you have it.\n" +
                "4. Never buy what you do not want, because it is cheap; it will be dear to you.\n" +
                "5. Pride costs us more than hunger, thirst, and cold.\n" +
                "6. We never repent of having eaten too little.\n" +
                "7. Nothing is troublesome that we do willingly.\n" +
                "8. How much pain have cost us the evils which have never happened.\n" +
                "9. Take things always by their smooth handle.\n" +
                "10. When angry, count ten, before you speak; if very angry, a hundred.";
        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("./data/ThomasJefferson.txt"))) {
            rewrite.write(overwriteText);
            rewrite.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        writePresidentOne();
        writePresidentTwo();
        writePresidentThree();
        writePresidentFour();
        writePresidentFive();
        System.out.println("Write complete.");
    }
}
