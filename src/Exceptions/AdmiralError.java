package Exceptions;

import java.util.Scanner;

public class AdmiralError {

    public static void main(String[] args) {
        int numerator;
        int denominator;
        Scanner userInput = new Scanner(System.in);
        try {
            System.out.println("input numerator:");
            numerator = Integer.parseInt(userInput.next());
            System.out.println("input denominator");
            denominator = Integer.parseInt(userInput.next());
            System.out.println(numerator + " / " + denominator + " = " + numerator/denominator);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("- - - End of Error Handling Example - - -");
        }

    }
}
