package Exceptions;

import java.util.Scanner;

public class Bonus {
    public static void main(String[]args){
        Scanner in = new Scanner(System.in);
        System.out.print("Convert a binary back to decimal: ");
        String send = in.next();
        System.out.println(getBinary(send));
        System.out.print("Convert a hex back to decimal: ");
        String sendHex = in.next();
        System.out.println(getBinary(sendHex));
    }
    public static int getBinary(String number){
        try {
            int check = Integer.parseInt(number);

        }catch (NumberFormatException bad){
            System.out.println("couldn't parse an integer out of that");
        }

        try {
            int check = Integer.valueOf(number,2);
        }
        catch (NumberFormatException bad){
            System.out.println("Not a binary number");
        }
        return Integer.valueOf(number,2);
    }
    public static int getHex(String number){
        try {
            int check = Integer.parseInt(number);
        }catch (NumberFormatException bad){
            System.out.println("couldn't parse an integer out of that");
        }
        return Integer.valueOf(number,16);
    }
}
