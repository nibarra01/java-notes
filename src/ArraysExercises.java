import java.util.Scanner;

public class ArraysExercises {
    public static void main(String[] args) {
        /*
        What happens when you run the following code? Why is Arrays.toString necessary?
int[] numbers = {1, 2, 3, 4, 5};
System.out.println(numbers);
         */
//        int[] numbers = {1, 2, 3, 4, 5};
//        System.out.println(numbers);

        /*
        [I@3cb5cdba
        The array itself isn't human readable, only the elements inside are?
        */

        Scanner in = new Scanner(System.in);


        System.out.println("Program main menu:\n" +
                "A. Array Exercise 1\n" +
                "B. Array Exercise 1 'st','nd','rd','th' placement demo\n" +
                "C. Array Exercise 2\n" +
                "D. Array Exercise 3\n");
        String menu = in.next().toUpperCase();

        switch (menu){
            case "A":
                if (rtnTrue6()){
                    System.out.println("You satisfied the conditions to make the method return true");
                }else {
                    System.out.println("You did not meet the conditions and the method returned false");
                }
                break;
            case "B":
                correctEndingDemo();
                break;
            case"C":
                System.out.println("Enter index 0 for the new array (data type: int) :");
                int c1 = in.nextInt();
                System.out.println("Enter index 1 for the new array (data type: int) :");
                int c2 = in.nextInt();
                System.out.println("Enter index 2 for the new array (data type: int) :");
                int c3 = in.nextInt();

                int c4 = sumArray(c1,c2,c3);
                System.out.println("The sum of the elements in the array is :" + c4);

                break;

            case "D":
                int[] exercise3 = new int[3];
                int[] returnedArray = reverseArray();

                int n1 = exercise3.length;
                int n2 = returnedArray.length;
                int n3;
                if (n1 > n2) {
                    n3 = n2;
                } else {
                    n3 = n1;
                }
                int n4 = n3;

                for (int x = 0; x < n3; x++) {
                    exercise3[x] = returnedArray[n4-1];
                    n4--;
                }
                System.out.print("\nThe reversed array is: ");
                for (int i : exercise3) {
                    System.out.print(i + " ");
                }


//                int y = 3;
//                int z = 3;
//                for (int x = 0; x < z ;x++){
//                    System.out.println(x + " " + (y-1));
//                    y--;
//                }

                break;



            default:
                break;

        }




    }


    public static boolean rtnTrue6(){
        Scanner sc = new Scanner(System.in);
        System.out.println("How long of an array would you like to make?");
        int arrayLength = sc.nextInt();
        int[] returnedArray = new int[arrayLength];

        for (int x = 0; x <arrayLength; x++){
            switch (x){
                case 0:
                    System.out.println("Enter the int value for the first index");
                    returnedArray[x] = sc.nextInt();
                    break;
                case 1:
                    System.out.println("Enter the int value for the second index");
                    returnedArray[x] = sc.nextInt();
                    break;
                case 2:
                    System.out.println("Enter the int value for the third index");
                    returnedArray[x] = sc.nextInt();
                    break;
                default:
                    String xs = Integer.toString(x+1);
                    if (xs.endsWith("1") && !xs.endsWith("11")){
                        System.out.println("Enter the value for the "+(x+1)+"st index");
                        returnedArray[x] = sc.nextInt();
                    } else if (xs.endsWith("2") && !xs.endsWith("12")){
                        System.out.println("Enter the value for the "+(x+1)+"nd index");
                        returnedArray[x] = sc.nextInt();
                    } else if (xs.endsWith("3") && !xs.endsWith("13")){
                        System.out.println("Enter the value for the "+(x+1)+"rd index");
                        returnedArray[x] = sc.nextInt();
                    }else {
                        System.out.println("Enter the value for the "+(x+1)+"th index");
                        returnedArray[x] = sc.nextInt();
                    }

                    break;
            }
        }

        return returnedArray[0] == 6 || returnedArray[arrayLength - 1] == 6;

    }

    public static int sumArray(int a, int b, int c){
        int[] everybodyLovesArraymond = {a,b,c};
        return everybodyLovesArraymond[0] + everybodyLovesArraymond[1] + everybodyLovesArraymond[2];
    }

    public static int[] reverseArray(){
        int [] backwards = new int[3];
        Scanner rennacS = new Scanner(System.in);
        System.out.println(".0 xedni rof eulav regetni eht retnE");
        backwards[0] = rennacS.nextInt();
        System.out.println(".1 xedni rof eulav regetni eht retnE");
        backwards[1] = rennacS.nextInt();
        System.out.println(".2 xedni rof eulav regetni eht retnE");
        backwards[2] = rennacS.nextInt();

        return backwards;
    }




    public static void correctEndingDemo(){
        Scanner sc = new Scanner(System.in);
        System.out.println("How long of an array would you like to make?");
        int arrayLength = sc.nextInt();

        for (int x = 0; x <arrayLength; x++){
            switch (x){
                case 0:
                    System.out.println("Enter the int value for the first index");
                    break;
                case 1:
                    System.out.println("Enter the int value for the second index");
                    break;
                case 2:
                    System.out.println("Enter the int value for the third index");
                    break;
                default:
                    String xs = Integer.toString(x+1);
                    if (xs.endsWith("1") && !xs.endsWith("11")){
                        System.out.println("Enter the value for the "+(x+1)+"st index");
                    } else if (xs.endsWith("2") && !xs.endsWith("12")){
                        System.out.println("Enter the value for the "+(x+1)+"nd index");
                    } else if (xs.endsWith("3") && !xs.endsWith("13")){
                        System.out.println("Enter the value for the "+(x+1)+"rd index");
                    }else {
                        System.out.println("Enter the value for the "+(x+1)+"th index");
                    }

                    break;
            }
        }



    }
}
