package AbstractAndInterface;

public class Main {
    public static void main(String[] args) {
        Wrestler wrestler1 = new JohnCena();
        WWE wrestler2 = new StoneCold();

        wrestler1.wrestlerName();
        wrestler1.finisher();
        wrestler1.paymentForPerformance(5);

        wrestler2.wrestlerName();
        wrestler2.finisher();
        wrestler2.paymentForPerformance(3);
    }
}
