package AbstractAndInterface;
/*
Abstract class - a special type of class that is created strictly to be a 'base class'
for other classes to derive from
- cannot be instantiated
- may have fields and methods just like classes
- they have abstract methods
    -abstract methods - methods that have no body and must be implemented in the derived class
    - only exist in abstract class
    - doesn't contain a body

    How to make an abstract class?
    How to make a class, an abstract class?
    - use the 'abstract' keyword inside of the class declaration
 */

public abstract class Wrestler {

    //method
    public static void paymentForPerformance(int hours){
        System.out.println("The Wrestler's pay for tonight's performance: " + hours*250);
    }

    //abstract method
    abstract public void wrestlerName();
    public abstract void themeMusic();
    public abstract void finisher();
}
