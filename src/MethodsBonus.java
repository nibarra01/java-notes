import java.util.InputMismatchException;
import java.util.Scanner;

public class MethodsBonus {
    public static void main(String[] args) {
        ChooseBonus();
//d4();
//        DiceGame();
    }

    public static void DiceGame(){
        System.out.println("====== DICE ROLL ======");
        boolean repeat = false;
        Scanner diceScanner = new Scanner(System.in);
        do {
        System.out.println();
        System.out.println("Choose a die type to roll:\n" +
                "a) d4\n" +
                "b) d6\n" +
                "c) d8\n" +
                "d) d12\n" +
                "e) d20\n" +
                "f) d100\n" +
                "g) custom\n");
        String whichDie = diceScanner.next().toLowerCase();
        switch (whichDie) {
            case "a":
                repeat = false;
                d4(4);
                break;
//                actually, I can make a few tweaks and just use the same method for each one.
            case "b":
                repeat = false;
//                d6();
                d4(6);
                break;
            case "c":
                repeat = false;
//                d8();
                d4(8);
                break;
            case "d":
                repeat = false;
//                d12();
                d4(12);
                break;
            case "e":
                repeat = false;
//                d20();
                d4(20);
                break;
            case "f":
                repeat = false;
//                d100();
                d4(100);
                break;
            case "g":
//                dUser();
                System.out.println("What size custom die?:");
                String userDieS = IntCheck();
                int  userDie = 0;
                if (userDieS.equals("failed")) {
                    do {
                        System.out.println("\nInput positive integer:");
                        userDieS = IntCheck();
                        if (userDieS.equals("failed")){
                            repeat = true;
                        } else {
                            userDie = Integer.parseInt(userDieS);
                            repeat = userDie <= 0;
                        }
                    } while (repeat);
                }
                if (userDie == 0){
                    userDie = Integer.parseInt(userDieS);
                }

                d4(userDie);

                repeat = false;
                break;
            default:
                System.out.println("User input error");
                repeat = true;

        }
        }while (repeat);



    }

// ================= Validation method =================================
    public static String IntCheck(){
        Scanner sc = new Scanner(System.in);
        String num = sc.next();
        try{
            Integer.parseInt(num);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "failed";
        }
        return num;
    }
//    ============================================================

//    ==================== Dice Roll ===========================

    public static int rollDie(int roll){
        return (int)(Math.random() * roll) + 1;
    }

    //    ============================================================


    public static void d4(int dieSize){
        System.out.println("Selected die: d" + dieSize);
        System.out.println();
        boolean repeat = false;
        Scanner d4scan = new Scanner(System.in);
        int numberOfDice;
        System.out.println("Roll how many d" + dieSize +"?:");
        String numberOfDiceS = IntCheck();

        // validate input
        if (numberOfDiceS.equals("failed")) {
            do {
                System.out.println("\nInput needs to be an integer.\nRoll how many d" + dieSize + "?:");
                numberOfDiceS = IntCheck();
                repeat = numberOfDiceS.equals("failed");
            } while (repeat);
        }

        numberOfDice = Integer.parseInt(numberOfDiceS);
        int[] rolls = new int[numberOfDice];

        for (int x = 0; x < numberOfDice; x++){
            rolls[x] = rollDie(dieSize);
            System.out.println("Roll " + (x+1) + ": " + rolls[x]);
        }

        //end

        int failCount = 0;
        do {
            System.out.println("\n Program Completed\n" +
                    "a) roll more of this die type\n" +
                    "b) return to die select menu\n" +
                    "c) return to top menu\n" +
                    "q) quit");
            String end = d4scan.next().toLowerCase();
            switch (end) {
                case "a":
                    d4(dieSize);
                    repeat = false;
                    break;
                case "b":
                    DiceGame();
                    repeat = false;
                    break;
                case "c":
                    ChooseBonus();
//                    System.out.println("not yet implemented");
                    repeat = false;
                    break;
                case "q":
                    System.out.println("Program ends.");
                    repeat = false;
                    break;
                default:
                    if (failCount < 5){
                        System.out.println("Invalid input");
                        failCount++;
                        repeat = true;
                    } else {
                        System.out.println("End of line.");
                        repeat = false;
                        break;
                    }
            }
        } while (repeat);
    }

// ==============================================================================

    public static void HighLow(){
        System.out.println("====== HIGH-LOW ======");
//        roll a d100
        System.out.println("Picking a number...");
        int theNumber = rollDie(100);
        System.out.println("A number between 1 and 100 has been selected.");
        boolean victory = false;
        Scanner hlsc = new Scanner(System.in);
        int HP = 15;
        int tries = 0;
        String guessS = "";
        int guess = -1;
        boolean repeat = false;
        int invalidCount = 0;
        boolean endGame = false;

        System.out.println("Start guessing\nAttempts remaining: "+ HP);
        do{
            guessS = IntCheck();
            // validate input
            if (guessS.equals("failed")) {
                do {
                    if (!guessS.equals("stop")) {
                        System.out.println("The input was not an integer");
                        System.out.println();
                        guessS = IntCheck();
                    }
                    if (guessS.equals("failed")){
//                        System.out.println(repeat);
//                        System.out.println(invalidCount);
                        repeat = true;
                        invalidCount++;
                    }
                    if (!guessS.equals("stop") && !guessS.equals("failed")){
                        repeat = false;
                    }
                    if (invalidCount >= 3){
//                        System.out.println(repeat);
//                        System.out.println(invalidCount);
                        invalidCount = 0;
                        System.out.println("Too many invalid inputs. -1 attempt");
                        HP--;
                        System.out.println("Attempts remaining: " + HP);
                    }
                    if (HP <= 0){
                        repeat = false;
                        guessS = "stop";
                    }

                } while (repeat);
            }


            //check the guess if wrong inputs didn't reduce HP to 0
            if (HP > 0) {
                guess = Integer.parseInt(guessS);
                if (guess == theNumber) {
                    System.out.println("GOOD GUESS!");
                    victory = true;
                    endGame = true;
                } else if (guess < theNumber) {
                    System.out.println("HIGHER");
                    HP--;
                    tries++;
                } else if (guess > theNumber) {
                    System.out.println("LOWER");
                    HP--;
                    tries++;
                }
            }

            if (HP <= 0){
                endGame = true;
            }
            if (!victory) {
                System.out.println("Attempts remaining: " + HP);
//            System.out.println(endGame);
            }

        } while (!endGame);

        //results
        if (victory){
            System.out.println("You win!");
            if (tries == 0){
                System.out.println("You won on the first guess!");
            } else {
                System.out.println("You won in " + tries + " guesses.");
            }
        }
        if (!victory){
            System.out.println("You were unable to guess THE NUMBER.\nGAME OVER");
        }

        System.out.println("THE NUMBER was: " + theNumber);

        int failCount = 0;
        do {
            System.out.println("\n Program Completed\n" +
                    "a) Play again\n" +
                    "b) Return to main menu\n" +
                    "q) quit");
            String end = hlsc.next().toLowerCase();
            switch (end) {
                case "a":
                    HighLow();
                    repeat = false;
                    break;
                case "b":
                    ChooseBonus();
                    repeat = false;
                    break;
                case "q":
                    System.out.println("Program ends.");
                    repeat = false;
                    break;
                default:
                    if (failCount < 5){
                        System.out.println("Invalid input");
                        failCount++;
                        repeat = true;
                    } else {
                        System.out.println("End of line.");
                        repeat = false;
                        break;
                    }
            }
        } while (repeat);


    }




    public static void ChooseBonus(){
        int failCount = 0;
        boolean repeat = false;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("\nCHOOSE!\n" +
                    "a) Roll some dice\n" +
                    "b) Play high-low\n" +
                    "q) quit");
            String ABQ = scanner.next().toLowerCase();
            switch (ABQ) {
                case "a":
                    DiceGame();
                    repeat = false;
                    break;
                case "b":
                    HighLow();
                    repeat = false;
                    break;
                case "q":
                    System.out.println("Program ends.");
                    repeat = false;
                    break;
                default:
                    if (failCount < 5){
                        System.out.println("Invalid input");
                        failCount++;
                        repeat = true;
                    } else {
                        System.out.println("End of line.");
                        repeat = false;
                        break;
                    }
            }
        } while (repeat);
    }
}
