package Contacts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public interface fileInteraction {
    static boolean validate(){
        String line1;
        String line2;
        BufferedReader br1 = null;
        BufferedReader br2= null;
        int counter = 0;
        int counter2 = 0;
        int error = 0;


        try {
            br1 = new BufferedReader(new FileReader("./data/SilentCal.txt"));
            br2 = new BufferedReader(new FileReader("./data/ThomasJefferson.txt"));
            line1 = br1.readLine();
            line2 = br2.readLine();

            while (line1 != null) {
//                System.out.println(line1 + "\t | \t" + line2);
                line1 = br1.readLine();
                counter++;
            }

            while (line2 != null){
                line2 = br2.readLine();
                counter2++;
            }
        } catch (IOException e) {
            System.out.println("Message: " + e.getMessage() + "| catch1 |" + "Caused by: " +  e.getCause());
            error++;
        } finally {
            try{
                if (br1 != null){
                    br1.close();
                }
                if (br2 != null){
                    br2.close();
                }
            }
            catch ( IOException e){
                System.out.println("Message: " + e.getMessage() + "| catch2 | " + "Caused by: " + e.getCause());
                error++;
            }
            System.out.println(error);
//            System.out.println(counter);
        }
        System.out.println(counter);
        System.out.println(counter2);

        return counter == counter2;

    }
}
