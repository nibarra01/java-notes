package Polymorphism;
/*
Polymorphism is a feature of Inheritance that allows us to treat object(s) of different
subclasses/ child classes that have the SAME superclass / parent class
as if they were of the superclass type

Methods or variables that  are defined with a superclass type
can accept objects that are a subclass of that type

FINAL keyword
- used to prevent inheritance and/or overriding
 */

public class ScrumMaster extends Developer{
    public String work(){
        return "I'm managing developers from home. HAHA";
    }
}
