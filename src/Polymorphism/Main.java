package Polymorphism;

public class Main {
    public static void main(String[] args) {
//        create a new developer object
//        Developer developer1 = new Developer();
//        Developer developer2 = new Developer();
//        Developer developer3 = new Developer();
//        Developer developer4 = new Developer();

//        doingWork(developer1);
//        doingWork(developer2);
//        doingWork(developer3);
//        doingWork(developer4);


//        polymorphism
        Developer[] codebound = new  Developer[5];

        codebound[0] = new Developer();
        codebound[1] = new ScrumMaster();
        codebound[2] = new ScrumMaster();
        codebound[3] = new Developer();
        codebound[4] = new Developer();

        for (Developer d: codebound){
            doingWork(d);
        }




    }
    public static void doingWork(Developer dev){
        System.out.println(dev.work());
    }
}
