package InheritanceGym;

import java.util.Scanner;

public abstract class Member {
    public String welcome = "Welcome to Inherit Fitness";
    protected double annualFee;
    private String name;
    private int memberID;
    private int memberSince;
    private int discount;

    public Member(String name, int memberID, int memberSince) {
        this.name = name;
        this.memberID = memberID;
        this.memberSince = memberSince;
//        System.out.println("parent constructor with 3 parameters");
    }
    public Member() {
//        System.out.println("parent constructor with no parameters");
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount() {
        System.out.println("Set discount for " + this.name);
        String adminPassword = "password123";
        Scanner Discount = new Scanner(System.in);
        System.out.print("\nPlease enter the admin password: ");
        String in = Discount.next();
        if (in.equals(adminPassword)){
            System.out.println("\nPlease enter the discount: ");
            int dsc = Discount.nextInt();
            this.discount = dsc;
        } else {
            System.out.println("Invalid password");
        }
    }

    public void displayMemberInfo(){
        System.out.println("\n====================" +
                "\nName: \t" + this.name +
                "\nMember ID: \t" + this.memberID +
                "\nMember since: \t" + this.memberSince +
                "\nAnnual fee: \t" + this.annualFee +
                "\n====================");
    }

//    public void calculateAnnualFee(){
//        this.annualFee = 0;
//    }
    public abstract void calculateAnnualFee();
}
