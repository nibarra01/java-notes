package Inheritance;
//how do you inherit from another class?
//use the 'extends' keyword

import javax.xml.transform.Source;

public class Dog extends Animal{
//    fields for our dog class
    private int eyes;
    private int legs;
    private int tail;
    private int teeth;
    private String fur;
    boolean isAGoodBoy;

// constructor
    public Dog(String name, int brain, int body, int size, int weight, int eyes, int legs, int tail, int teeth, String fur, boolean isAGoodBoy) {
        super(name, brain, body, size, weight); //this line bust be first in the child's constructor
//        this is initializing the base/parent/super characterestics of an animal

        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.fur = fur;
        this.isAGoodBoy = true;
    }

    //methods
    private void chew(){
        System.out.println("this dog is chewing his food");
    }

    //overriding methods - inherit a method but make it unique for our class
    @Override
    public void eat(){
        System.out.println("this dog is eating");
        chew();
        super.eat();
        //super keyword: a- allows us to access a superclasses method and constructors from within a subclass
    }
}
