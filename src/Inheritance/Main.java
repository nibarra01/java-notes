package Inheritance;

public class Main {
    public static void main(String[] args) {
//        create an animal object
        Animal  x = new Animal("Animal",1, 1, 5, 10);

        //create a new dog object
        Dog dog1 = new Dog("Chula",1,1,5,20, 2,4,1,25,"long sliky",true);
        dog1.eat();
        x.eat();
    }
}
