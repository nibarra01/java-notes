package Inheritance;

public class Animal {
    //INHERITANCE - one of the key concepts in OOP
//     allows us to crate a new class from an existing class so that we can reuse code
//    oop really allows us to create classes to inherit the behavior from other classes

    //    fields / variables /properties
//    - can be declared as public, private or protected
//    creating the characteristics of things all animals have
    private String name;
    private int brain;
    private int body;
    private int size;
    private int weight;

    //    constructor
    public Animal(String name, int brain, int body, int size, int weight) {
        this.name = name;
        this.brain = brain;
        this.body = body;
        this.size = size;
        this.weight = weight;
    }

    // method
    public void eat() {
        System.out.println("this animal is eating");
    }

    public void move() {
        System.out.println("this animal is moving");
    }

//getter / setter methods
//    our fields/variables are declared as private
//  means the fields cannot be accessed outside this class


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBrain() {
        return brain;
    }

    public void setBrain(int brain) {
        this.brain = brain;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

}
