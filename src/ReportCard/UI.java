package ReportCard;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public interface UI {
    public static void initiateUI(HashMap<String, Student> classList) throws InterruptedException, IOException {
        System.out.print("\nLoading\r");
        Thread.sleep(200);
        System.out.print("Loading.\r");
        Thread.sleep(200);
        System.out.print("Loading..\r");
        Thread.sleep(200);
        System.out.print("Loading...\r");
        Thread.sleep(200);
        System.out.print("Loading....\r");
        Thread.sleep(200);
        System.out.print("Loading....\r");

        boolean repeat = false;
        boolean quit = false;
        Thread.sleep(50);
        Scanner in = new Scanner(System.in);
        String entry;
        do {
            System.out.println("┌─────────────────────────┐");
            System.out.println("│ Student Roster Database │");
            System.out.println("├────────────┬────────────┴──────────┐");
            System.out.println("│  Student   │        Username       │");
            System.out.println("├────────────┼───────────────────────┤");

            for (Map.Entry<String, Student> x : classList.entrySet()) {
                //reuired type Entry
                //" Raw use of parameterized class 'Map.Entry' "
                System.out.print("│ ");
                System.out.print(x.getValue().getName());
                if (x.getValue().getName().length() < 6) {
                    System.out.print("\t\t │ ");
                } else {
                    System.out.print("\t │ ");
                }
                System.out.print(x.getKey());
                if (x.getKey().length() <= 12) {
                    System.out.print("\t\t\t │\n");
                } else if (x.getKey().length() < 17) {
                    System.out.print("\t\t │\n");
                } else {
                    System.out.print("\t │\n");
                }
            }
            System.out.print("├────────────┴───────────────────────┤\n");
            System.out.print("│              Q: Quit               │\n");
            System.out.print("└────────────────────────────────────┘\n");


            System.out.println("Select a student using the student's name or username");
            entry = in.next();
            switch(entry){
                case "Paladin":
                case "lay0nh4nds":
                    System.out.println("Student name: " + classList.get("lay0nh4nds").getName());
                    System.out.println("Student grades: " + classList.get("lay0nh4nds").getGrades());
                    System.out.println("Student average: " + classList.get("lay0nh4nds").getAverage());
                    repeat = false;
                    break;
                case "Fighter":
                case "hitwithsword":
                    System.out.println("Student name: " + classList.get("hitwithsword").getName());
                    System.out.println("Student grades: " + classList.get("hitwithsword").getGrades());
                    System.out.println("Student average: " + classList.get("hitwithsword").getAverage());
                    repeat = false;
                    break;
                case "Rogue":
                case "sn3akAtt4ckd8":
                    System.out.println("Student name: " + classList.get("sn3akAtt4ckd8").getName());
                    System.out.println("Student grades: " + classList.get("sn3akAtt4ckd8").getGrades());
                    System.out.println("Student average: " + classList.get("sn3akAtt4ckd8").getAverage());
                    repeat = false;
                    break;
                case "Cleric":
                case "PelorSunnyD":
                    System.out.println("Student name: " + classList.get("PelorSunnyD").getName());
                    System.out.println("Student grades: " + classList.get("PelorSunnyD").getGrades());
                    System.out.println("Student average: " + classList.get("PelorSunnyD").getAverage());
                    repeat = false;
                    break;
                case "Wizard":
                case "LocateCityNuke":
                    System.out.println("Student name: " + classList.get("LocateCityNuke").getName());
                    System.out.println("Student grades: " + classList.get("LocateCityNuke").getGrades());
                    System.out.println("Student average: " + classList.get("LocateCityNuke").getAverage());
                    repeat = false;
                    break;
                case "Bard":
                case "CHAisNotADumpStat":
                    System.out.println("Student name: " + classList.get("CHAisNotADumpStat").getName());
                    System.out.println("Student grades: " + classList.get("CHAisNotADumpStat").getGrades());
                    System.out.println("Student average: " + classList.get("CHAisNotADumpStat").getAverage());
                    repeat = false;
                    break;
                case "Q":
                case "q":
                    repeat = false;
                    quit = true;
                    break;
                default:
                    System.out.println("No match found (entry field is case sensitive).");
                    System.out.println("Press [enter] to continue.");
                    System.in.read();
                    in.nextLine();
                    repeat = true;
                    break;
            }

            if (!repeat && !quit){
                boolean repeat2 = false;
                in.nextLine();
                do {
//                    in.nextLine();
                    System.out.println("Check information on another student? [Y/N]");
                    String check = in.nextLine().toUpperCase();
                    switch (check) {
                        case "Y":
                        case "YES":
                            repeat = true;
                            repeat2 = false;
                            break;
                        case "N":
                        case "NO":
                            repeat = false;
                            repeat2 =false;
                            break;
                        default:
                            System.out.println("invalid input");
                            repeat2 = true;
                            break;
                    }
                }while (repeat2);
            }
        }while(repeat);

        System.out.print("\nLogging out...\r");
        Thread.sleep(110);
        System.out.print("ogging out...\r");
        Thread.sleep(110);
        System.out.print("gging out...\r");
        Thread.sleep(110);
        System.out.print("ging out...\r");
        Thread.sleep(110);
        System.out.print("ing out...\r");
        Thread.sleep(110);
        System.out.print("ng out...\r");
        Thread.sleep(110);
        System.out.print("g out...\r");
        Thread.sleep(110);
        System.out.print(" out...\r");
        Thread.sleep(110);
        System.out.print("out...\r");
        Thread.sleep(110);
        System.out.print("ut...\r");
        Thread.sleep(110);
        System.out.print("t...\r");
        Thread.sleep(110);
        System.out.print("...\r");
        Thread.sleep(110);
        System.out.print("..\r");
        Thread.sleep(110);
        System.out.print(".\r");
        Thread.sleep(110);
        System.out.print(" \r");
        Thread.sleep(110);
        System.out.println("\n\n\n");
        System.out.println("Logout complete. Program closed.");
    }
}
