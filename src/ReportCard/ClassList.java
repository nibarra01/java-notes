package ReportCard;

import java.util.HashMap;

public class ClassList{
    public static HashMap<String, Student> getClassList(){
        Student student0001 = new Student("Fighter");
        Student student0002 = new Student("Wizard");
        Student student0003 = new Student("Cleric");
        Student student0004 = new Student("Rogue");
        Student student0005 = new Student("Paladin");
        Student student0006 = new Student("Bard");

        HashMap<String, Student> classList = new HashMap<>();
        classList.put("hitwithsword",student0001);
        classList.put("LocateCityNuke",student0002);
        classList.put("PelorSunnyD",student0003);
        classList.put("sn3akAtt4ckd8",student0004);
        classList.put("lay0nh4nds",student0005);
        classList.put("CHAisNotADumpStat",student0006);

        return classList;
    }

}
