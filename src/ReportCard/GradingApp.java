package ReportCard;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {
    public static void main(String[] args) throws InterruptedException, IOException {
        HashMap<String, Student> students = ClassList.getClassList();
        Grades.setGrades(students);
        UI.initiateUI(students);
    }
}
