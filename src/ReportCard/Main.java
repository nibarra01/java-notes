package ReportCard;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Student test = new Student("Test student");
        test.addGrade(100);
        test.addGrade(50);
        System.out.println(test.getAverage());
        System.out.println(test.getName());


        HashMap<String, Student> students = new HashMap<>();
        Student student0001 = new Student("n1");
        Student student0002 = new Student("n2");
        Student student0003 = new Student("n3");
        Student student0004 = new Student("n4");
        Student student0005 = new Student("n5");

        students.put("g1", student0001);
        students.put("g2", student0002);
        students.put("g3", student0003);
        students.put("g4", student0004);
        students.put("g5", student0005);

        System.out.println(students.keySet());
        System.out.println(students.size());
        for (String x : students.keySet()) {
            System.out.println(x);
        }
        for (Student x : students.values()) {
            System.out.println(x.getName());
        }

        Scanner scanner = new Scanner(System.in);
        String testInput = scanner.next();

        System.out.println(students.containsKey(testInput));

//        System.out.println(
//                students.get(testInput)
//        );
        System.out.println(
                students.get(testInput).getName()
        );

        students.get(testInput).addGrade(100);
        students.get(testInput).addGrade(90);
        students.get(testInput).addGrade(83);
        System.out.println(students.get(testInput).getAverage());


    }
}
