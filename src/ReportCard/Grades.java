package ReportCard;

import java.util.HashMap;

public interface Grades {

    public static void setGrades(HashMap<String, Student> classList){
        Student[] clArray = new Student[classList.size()];
        int x = 0;
        for(String s : classList.keySet()){
            clArray[x] = classList.get(s);
            x++;
        }



        for (Student s : clArray){
            switch (s.getName()){
                case "Fighter":
                    for (int i =0; i < fighterGrades.length; i++){
                        s.addGrade(fighterGrades[i]);
                    }
                    break;
                case "Wizard":
                    for (int i =0; i < wizardGrades.length; i++){
                        s.addGrade(wizardGrades[i]);
                    }
                    break;
                case "Cleric":
                    for (int i =0; i < clericGrades.length; i++){
                        s.addGrade(clericGrades[i]);
                    }
                    break;
                case "Rogue":
                    for (int i =0; i < rogueGrades.length; i++){
                        s.addGrade(rogueGrades[i]);
                    }
                    break;
                case "Paladin":
                    for (int i = 0 ; i < paladinGrades.length; i++){
                        s.addGrade(paladinGrades[i]);
                    }
                    break;
                case "Bard":
                    for (int i =0; i < bardGrades.length; i++){
                        s.addGrade(bardGrades[i]);
                    }
                    break;
            }
        }
    }

    int[] fighterGrades = {70, 80, 100, 74, 62};
    int[] wizardGrades = {100, 100, 0, 100, 105};
    int[] clericGrades = {87, 85, 89, 90, 97};
    int[] rogueGrades = {100, 99, 97, 95, 102};
    int[] paladinGrades = {100, 99, 97, 95, 102};
    int[] bardGrades = {95, 92, 83, 86, 101};
}
