package ReportCard;

import java.util.ArrayList;

public class Student {
    private String name;
    private ArrayList<Integer> grades;

    public Student(String name){
        this.name = name;
        this.grades = new ArrayList<>();
    }

    public String getName(){
        return this.name;
    }

    public void addGrade(int grade){
        grades.add(grade);
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public double getAverage(){
        int sum = 0;
        int assignments = grades.size();
        for(int x : grades){
            sum += x;
        }
        return (double)sum / assignments;
    }
}
