import InheritanceGym.Member;
import InheritanceGym.NormalMember;
import InheritanceGym.VIPMember;

public class GymManagement {
    public static void main(String[] args) {
//        NormalMember mbr1 = new NormalMember("Jack Johnson",1,10132020);
//        VIPMember mbr2 = new VIPMember("John Jackson",2,10132020);
//
//        mbr1.calculateAnnualFee();
//        mbr2.calculateAnnualFee();
//
//        mbr1.displayMemberInfo();
//        mbr2.displayMemberInfo();

        Member[] initialMemberList = new Member[7];

        for (int n = 0; n <= 6; n++){
            if (n <= 2){
                initialMemberList[n] = new NormalMember("Clone Trooper "+(MethodsBonus.rollDie(10000)), n+1, 10012020);
            }
            else if (n > 2 && n <= 5) {
                initialMemberList[n] = new VIPMember("Clone Trooper " + (MethodsBonus.rollDie(10000)), n+1, 9152020);
            }
            else{
                initialMemberList[n] = new NormalMember("Clone Trooper "+(MethodsBonus.rollDie(10000)),n+1,10132020);
            }
        }

        initialMemberList[0].setDiscount();
        initialMemberList[3].setDiscount();
        initialMemberList[5].setDiscount();

        for (Member x : initialMemberList){
            x.calculateAnnualFee();
            x.displayMemberInfo();
        }

    }
}
