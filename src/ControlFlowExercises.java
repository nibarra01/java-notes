import java.util.Scanner;

public class ControlFlowExercises {
    public static void main(String[] args) {
//
////        int i = 9;
////        while (i <= 23){
////            System.out.println(i);
////            i++;
////        }
////
////        int n = 0;
////        do {
////            System.out.println(n);
////            n +=2;
////
////        } while (n <= 100);
//
//
////    int l = 100;
////        do {
////        System.out.println(l);
////        l -=5;
////
////    } while (l >= -10);
//
//
////
        double w = 2;
        do {
            System.out.format("%1.0f\n",w);
//            System.out.println(w*w);

//            int x = w;
//            int y = w;
//            w = x * y;

            w *= w;

//            w = Math.pow(w, 2);

        }while (w <= 1000000);
//
//
//
//        for (var x = 100; x >= -10; x = x - 5) {
//        System.out.println(x);
//
//    }
//
//
//        for (double x = 2; x <= 1000000; x *= x){
//            System.out.format("%1.0f\n",x);
//        }
//


        /*
        -------------------------------------------------------------------------------
         */

        for (var x = 1; x <= 100; x++) {
            if (x % 3 == 0 && x % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (x % 5 == 0) {
                System.out.println("Buzz");
            } else if (x % 3 == 0) {
                System.out.println("Fizz");
            } else {

                System.out.println(x);
            }
        }


        Scanner sc = new Scanner(System.in);
        System.out.println("Number table\nDraw table to which number:");
        int input = Integer.parseInt(sc.nextLine());
        System.out.println(" number | square | cube ");
        for (int x = 1; x <= input; x++){
            System.out.println("\t"+ x + "\t|\t" + (x*x) + "\t|\t" + (x*x*x));
        }

/*


        System.out.println("What number would you like to go up tp?");
        int num = sc.nextInt();
        System.out.println("Here is your table");
        System.out.println("NUMBER | SQUARE | CUBED");
        System.out.println("- - - -| - - - -| - - - -");
        for (int i = 1; i <= num; i++){
            int num1 = i;
            int num2 = (int)Math.pow(i,2);
            int num3 = (int)Math.pow(i,3);
          //  System.out.println("\t" + num1 + " | \t" + num2 + " | \t " + num3);
            System.out.printf("%-6d | %-6d | %-gd%n", num1, num2, num3);
        }

  */




        Scanner Input = new Scanner(System.in);
        boolean end = false;
        while (end == false){
            System.out.println("input grade (range: 0-100)");
            int grade = Input.nextInt();
            String letter;
            letter = "I'm going to initialize it later stop throwing an error";

            if (grade <= 100 && grade >= 90){
                letter = "A";
            }
            if (grade <= 89 && grade >= 80){
                letter = "B";
            }
            if (grade <= 79 && grade >= 70){
                letter = "C";
            }
            if (grade <= 69 && grade >= 60){
                letter = "D";
            }
            if (grade <= 59){
                letter = "F";
            }

            String bonus = Integer.toString(grade);
            String gradeFinal;
            if (bonus.endsWith("9") || bonus.endsWith("8") || bonus.equals("100")){
                gradeFinal = letter + "+";
            } else
            if (bonus.endsWith("3") || bonus.endsWith("2") || bonus.endsWith("1") || bonus.endsWith("0")){
                gradeFinal= letter + "-";
            } else {
                gradeFinal = letter;
            }
            if (gradeFinal.startsWith("F")){
                gradeFinal = "F";
            }

            switch (gradeFinal){
                case "A+":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "A":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "A-":
                    System.out.println("Grade: " + grade + " is an " + gradeFinal);
                    break;
                case "B+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "B":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "B-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "C-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D+":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "D-":
                    System.out.println("Grade: " + grade + " is a " + gradeFinal);
                    break;
                case "F":
                    System.out.println("Failure!");
                    break;
            }
            System.out.println("\nEnter another grade(Y/N)?:");
            String checkEnd = Input.next();
            checkEnd = checkEnd.toUpperCase();
            if (checkEnd.startsWith("N")){
                end = true;
            }
        }




        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number between 1 and 10");
        String number = scanner.next();
        switch (number){
            case "0":
                System.out.println("ZERO");
                break;
            case "1":
                System.out.println("ONE");
                break;
            case "2":
                System.out.println("TWO");
                break;
            case "3":
                System.out.println("THREE");
                break;
            case "4":
                System.out.println("FOUR");
                break;
            case "5":
                System.out.println("FIVE");
                break;
            case "6":
                System.out.println("SIX");
                break;
            case "7":
                System.out.println("SEVEN");
                break;
            case "8":
                System.out.println("EIGHT");
                break;
            case "9":
                System.out.println("NINE");
                break;
            case "10":
                System.out.println("TEN");
                break;
            default:
                System.out.println("OTHER");
        }

        Scanner userInput = new Scanner(System.in);
        System.out.println("enter a year (1-9999)");
        int year = userInput.nextInt();
        if (year < 1 || year > 9999){
            System.out.println("Number entered not within expected range");
        } else
        if (year % 400 == 0){
            System.out.println("Leap Year! :)");
        } else
        if (year % 4 == 0 && year % 100 != 0){
            System.out.println("Leap Year! :)");
        } else {
            System.out.println("Not a Leap Year :(");
        }



        }

    }



