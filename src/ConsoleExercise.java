import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {
        double pi = 3.14159;
        System.out.format("The value of pi is approximately %1.2f", pi);

        System.out.println();

//        Scanner userInput = new Scanner(System.in);
//        System.out.println("input integer");
//        int nextInt = userInput.nextInt();

//        Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
//    * What happens if you input something that is not an integer?

//        Exception in thread "main" java.util.InputMismatchException
//	at java.base/java.util.Scanner.throwFor(Scanner.java:939)
//	at java.base/java.util.Scanner.next(Scanner.java:1594)
//	at java.base/java.util.Scanner.nextInt(Scanner.java:2258)
//	at java.base/java.util.Scanner.nextInt(Scanner.java:2212)
//	at ConsoleExercise.main(ConsoleExercise.java:12)

//        System.out.println("input 3 words");
//        String word1 = userInput.next();
//        String word2 = userInput.next();
//        String word3 = userInput.next();
//        System.out.println("The three words are:\n" + word1 + "\n" + word2 + "\n" + word3);

//            * What happens if you enter less than 3 words?
//    * What happens if you enter more than 3 words?


//        nothing, returns are ignored and the system awaits 3 inputs
//        words after the third are ignored

//        System.out.println("input. more input.");
//        String newline = userInput.nextLine();

//        Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//    * do you capture all of the words?
//        no
//Rewrite the above example using the .nextLine method.

        Scanner userInput = new Scanner(System.in);

        System.out.println("input units (abbreviation)");
        String u = userInput.nextLine();
        System.out.println("input length");
        double l =Double.parseDouble(userInput.nextLine());
        System.out.println("input width");
        double w = Double.parseDouble(userInput.nextLine());
        System.out.println("input height");
        double h = Double.parseDouble(userInput.nextLine());
        System.out.println("The Perimeter is: " + ((2*l) + (2*w)) + u +"\n" + "and the Area is: " + l*w +u +"^2" + "\n" + "and the Volume is: " + l*w*h + u + "^3");




    }
}
