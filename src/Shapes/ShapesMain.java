package Shapes;

public class ShapesMain {
    public static void main(String[] args) {
        Measurable myShapeR = new Rectangle(16,9);
        Measurable myShapeS = new Square(5.5);


        System.out.println("\nRectangle area: " + myShapeR.getArea());
        System.out.println("Rectangle perimeter: " + myShapeR.getPerimeter());
        System.out.println("Rectangle length: " + myShapeR.getLength());
        System.out.println("Rectangle width: " + myShapeR.getWidth());
        System.out.println("\nSquare area: " + myShapeS.getArea());
        System.out.println("Square perimeter: " + myShapeS.getPerimeter());
        System.out.println("Square sides: " + myShapeS.getLength() + " | " + myShapeS.getWidth());

/*
Answer the following questions:
1. Why does the code fail to compile if you leave off the getPerimeter method in Rectangle?
    it didn't...?
2. What happens if you try to call the getLength or getWidth methods of the myShape variable? Why?
    There is an error that prevents the java app from building.
    The interface doesn't have a getLength or getWidth to override
 */
    }
}
