package Shapes;

public class Quadrilateral extends Shape implements Measurable {
    protected double length;
    protected double width;
    public Quadrilateral(double l, double w){
        this.length = l;
        this.width = w;
    }
    @Override
    public double getLength(){
        return this.length;
    }

    @Override
    public double getWidth(){
        return this.width;
    }

    @Override
    public double getPerimeter() {
        return (2 * this.length) + (2 * this.width);
    }

    @Override
    public double getArea() {
        return this.length * this.width;
    }
}
