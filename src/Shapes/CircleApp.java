package Shapes;

import validation.Input;

import java.util.Scanner;

public class CircleApp {
    public static void main(String[] args) {

        Scanner circleScanner = new Scanner(System.in);
        Input yn = new Input();
        boolean makeCircle;
        boolean quit;
        boolean makeAnotherCircle;
        int circlesMade = 0;

        System.out.println("Do you want to make a circle?");
        makeCircle = yn.yesNo();
        if (makeCircle){
            quit = false;
            System.out.println("What is the circle's radius?");
            double rad = circleScanner.nextDouble();
            Circle newCir = new Circle(rad);
            System.out.println("Your circle's Area is: " + newCir.getArea());
            System.out.println("Your circle's Circumference is: " + newCir.getCircumference());
            circlesMade++;


        } else {
            System.out.println("\nY-you too.");
            quit = true;
        }

        Circle newerCir = new Circle();
        if (!quit){
            System.out.println("Do you want to make another circle?");
            makeAnotherCircle = yn.yesNo();
            while (makeAnotherCircle){
                System.out.println("What is the new circle's radius?");
                double radder = circleScanner.nextDouble();
                newerCir.setRadius(radder);
                System.out.println("Your new circle's Area is: " + newerCir.getArea());
                System.out.println("Your new circle's Circumference is: " + newerCir.getCircumference());
                circlesMade++;


                System.out.println("Do you want to make another circle?");
                makeAnotherCircle = yn.yesNo();


                }

                System.out.println("Why not?");

        }

        switch (circlesMade){
            case 0:
                System.out.println("You created " + circlesMade + " circles... ;_;");
                break;
            case 1:
                System.out.println("You created one circle.");
                break;
            default:
                System.out.println("You created " + circlesMade + " circles!");
                break;

        }
    }
}
