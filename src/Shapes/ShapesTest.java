package Shapes;

public class ShapesTest {
    public static void main(String[] args) {
        Rectangle rec_box = new Rectangle(5,4);
        Rectangle rec_box2 = new Square(6);

        System.out.println("rec_box Area: " + rec_box.getArea());
        System.out.println("rec_box Perimeter: " + rec_box.getPerimeter());
        System.out.println();
        System.out.println("rec_box2 Area: " + rec_box2.getArea());
        System.out.println("rec_box2 Perimeter: " + rec_box2.getPerimeter());
    }
}
