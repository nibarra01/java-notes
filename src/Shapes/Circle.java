package Shapes;

public class Circle {
    private double radius;
    public Circle(double radius){
        this.radius = radius;
    }
    public Circle(){
//        System.out.println("Radius not set. Use setRadius(r) to set the radius.");
    }

    public void setRadius(double r) {
        this.radius = r;
    }

    public double getArea(){
        return Math.PI * Math.pow(this.radius, 2);
    }

    public double getCircumference(){
        return 2 * Math.PI * this.radius;

    }
}
