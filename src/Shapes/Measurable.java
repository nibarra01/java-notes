package Shapes;

public interface Measurable {
    double getPerimeter();
    double getArea();

    public double getLength();
    public double getWidth();
}
