package Objects;

public class Student {
    public String name;
    public String  cohort;

//    private instance variable
    private double grade;

    public Student(String studentName){
        name = studentName;
        cohort = "Unassigned";
    }

//    public Student(String studentName, String assignedCohort){
//        name = studentName;
//        cohort = assignedCohort;
//    }

//    public void StudentCohort(String cohort){
//        //this keyword provides us a way to refer to the current instance
//        this.cohort = cohort;
//    }

//    refactored by using the this keyword
public Student(String name, String cohort){
    this.name =name;
    this.cohort = cohort;
}

public String sayHello(){
        return "Hello from " + this.name + "!";
}

    public String getStudentInfo(){
        return String.format("Student Name: %s\nCohort Assigned: %s", name, cohort);
    }

    public Student(String name, String cohort, double grade){
        this.name = name;
        this.cohort = cohort;
        this.grade = grade;

    }

    public double shareGrade(){
        return grade;
    }
}
