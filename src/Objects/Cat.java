package Objects;

public class Cat {
//    instance variables: state(s) of the Cat class
    String name;
    int age;
    String color;
    String breed;

//    instance methods: "behaviors" of the Cat
    public static void  sleep(){
        System.out.println("kitty is sleep :3");
    }

    public static void  play(){
        System.out.println("kitty is playing :3");
    }

    public static void feed() {
        System.out.println("kitty is eats :3");
    }


    public String greeting(){
        return String.format("Hello this is %s", name);
    }


}
