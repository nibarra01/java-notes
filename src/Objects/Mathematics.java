package Objects;

public class Mathematics {
//    static fields are defined with the static keyword
    /*
    -are shared by all instances of the class
    - means that static properties should not be anything that is supposed to be unique to the instance(s) of the class
     */

    // static property
    public static double myPi = 3.14159;

    //static method
    public static int add(int x, int y){
        return x + y;
    }

    public static int multiply(int x, int y){
        return x * y;
    }
}
