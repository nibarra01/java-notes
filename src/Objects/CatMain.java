package Objects;

public class CatMain {
    public static void main(String[] args) {
//        create two objects of the Cat class
        Cat momo = new Cat();
        Cat thor = new Cat();

        //define their states and behaviors
        momo.name = "Momo";
        momo.age = 3;
        momo.breed = "Russian Blue";
        momo.color = "Brown";

        momo.sleep();

        //define Thor cat
        thor.name = "Thor";
        thor.age = 7;
        thor.breed = "Maine Coon";
        thor.color = "brown";

        thor.feed();

        System.out.println(thor.name); //Thor

        System.out.println(momo.greeting()); // Hello this is Momo
    }
}
