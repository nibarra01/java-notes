package validation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Input {
    private Scanner scanner;

    public Input(){
//        String ThisInstance = String.valueOf(this);
//        System.out.println("variable ThisInstance is: " + ThisInstance);

        //???
        this.scanner = new Scanner(System.in);
    }

    public String getString(){
        System.out.println("Input a string:");
        String var = this.scanner.nextLine();
//        System.out.println("previous input placed in new String 'var'");
        return var;
    }

    public boolean yesNo(){
        System.out.println("Entering a y or yes will cause this to return true");
        String var = this.scanner.next();
        return var.toUpperCase().equals("Y") || var.toUpperCase().equals("YES");
    }

    public int getInt(int min, int max){
        System.out.println("Hello this is getInt(). Please input integer.");
        String var = this.scanner.next();
        var = getIntCheck(var);
        if (var.equals("f")){
            do {
                System.out.println("integer not entered.\nInput integer between " + min + " and " + max + ":");
                var = this.scanner.next();
                var = getIntCheck(var);
            } while (var.equals("f"));
        }
        int returnedVar = Integer.parseInt(var);
        if (returnedVar < min){
            System.out.println("Input is below specified range\nInput integer between " + min + " and " + max + ":");
            returnedVar = getInt(min,max);
        } else if (returnedVar > max){
            System.out.println("Input is above specified range\nInput integer between " + min + " and " + max + ":");
            returnedVar = getInt(min,max);
        }

        // was not updating if wrong input was done before a correct one


        return  returnedVar;

    }

    private String getIntCheck(String test){
        try{
            Integer.parseInt(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "f";
        }
        return test;
    }

    public int getInt(){
        System.out.println("Hello this is getInt(). Please input integer.");
        String var = this.scanner.next();
        var = getIntCheck(var);
        if (var.equals("f")){
            do {
                System.out.println("integer not entered.\nInput integer:");
                var = this.scanner.next();
                var = getIntCheck(var);
            } while (var.equals("f"));
        }
        return Integer.parseInt(var);
    }

    public double getDouble(double mint, double gum){
        System.out.println("Hello this is getDouble(). Please input double.");
        String var = this.scanner.next();
        var = getDoubleCheck(var);
        if (var.equals("f")){
            do {
                System.out.println("double not entered.\nInput double between " + mint + " and " + gum + ":");
                var = this.scanner.next();
                getDoubleCheck(var);
            } while (var.equals("f"));
        }
        double returnedVar = Double.parseDouble(var);
        if (returnedVar < mint){
            System.out.println("Input is below specified range\nInput double between " + mint + " and " + gum + ":");
            returnedVar = getDouble(mint,gum);
        } else if (returnedVar > gum){
            System.out.println("Input is above specified range\nInput double between " + mint + " and " + gum + ":");
            returnedVar = getDouble(mint,gum);
        }
        return returnedVar;
    }

    private String getDoubleCheck(String test){
        try{
            Double.parseDouble(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "f";
        }
        return test;
    }

    public double getDouble(){
        System.out.println("Hello this is getDouble(). Please input double.");
        String var = this.scanner.next();
        getDoubleCheck(var);
        if (var.equals("f")){
            do {
                System.out.println("double not entered.\nInput double:");
                var = this.scanner.next();
                getDoubleCheck(var);
            } while (var.equals("f"));
        }
        return Double.parseDouble(var);
    }


}
