package validation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserTest {
    public static void main(String[] args) {
        int quitTimer = 3;
        boolean quit = false;
        Scanner user = new Scanner(System.in);
        Input testUser = new Input();
        boolean error = false;

        do {
            System.out.println("\n\nChoose method to test:" +
                    "\n1. getString()" +
                    "\n2. yesNo()" +
                    "\n3. getInt(int min, int max)" +
                    "\n4. getInt()" +
                    "\n5. getDouble(double min, double max)" +
                    "\n6. getDouble()" +
                    "\n7. [quit]");

            String Switch = user.next();
            switch (Switch) {
                case "1":

                    String case1 = testUser.getString();
                    System.out.println("getString() returned: '"+case1+"'");
                    break;
                case "2":
                    boolean case2 = testUser.yesNo();
                    System.out.println("yesNo() returned: '"+case2+"'");
                    break;
                case "4":
                    int case4 = testUser.getInt();
                    System.out.println("getInt() returned: '"+case4+"'");
                    break;
                case "6":
                    double case6 = testUser.getDouble();
                    System.out.println("getDouble() returned: '"+case6+"'");
                    break;
                case "7":
                    quit = true;
                    System.out.println("\nEnd of line.\n");
                    break;

                case "3":
                    int num1;
                    int num2;
                    System.out.println("Range not set for testing");
                    System.out.println("Define integer value for minimum");
                    String str1 = user.next();
                    str1 = IntCheck(str1);
//                    System.out.println(str1);
                    if (str1.equals("f")){
                        do {
                            System.out.println("Input validation works. Rejected invalid input for minimum integer value\nInput valid data now:");
                            str1 = user.next();
                            str1 = IntCheck(str1);
                            error = str1.equals("f");
                        }while (error);
                    }
                    System.out.println("Define integer value for maximum");
                    String str2 = user.next();
                    str2 = IntCheck(str2);
                    if (str2.equals("f")){
                        do {
                            System.out.println("Input validation works. Rejected invalid input for maximum integer value\nInput valid data now:");
                            str2 = user.next();
                            str2 = IntCheck(str2);
                            error = str2.equals("f");
                        }while (error);
                    }
                    num1 = Integer.parseInt(str1);
                    num2 = Integer.parseInt(str2);

                    if (num1 < num2) {
                        System.out.println("Running getInt() with range of [" + num1 + "]-[" + num2 + "]\n");
                        int case3 = testUser.getInt(num1, num2);
                        System.out.println("getInt(int min, int max) returned: '"+case3+"'");
                    } else if (num1 > num2){
                        System.out.println("Minimum provided was greater than Maximum provided");
                        System.out.println("Running getInt() with range of [" + num2 + "]-[" + num1 + "]\n");
                        int case3 = testUser.getInt(num2, num1);
                        System.out.println("getInt(int min, int max) returned: '"+case3+"'");
                    } else if (num1 == num2){
                        System.out.println("Values provided for minimum and maximum were the same.");
                        num1 = 0;
                        num2 = 10;
                        System.out.println("Running getInt() with range of [" + num1 + "]-[" + num2 + "]\n");
                        int case3 = testUser.getInt(num1, num2);
                        System.out.println("getInt(int min, int max) returned: '"+case3+"'");
                    }
                    break;

                case "5":
                    double num3;
                    double num4;
                    System.out.println("Range not set for testing");
                    System.out.println("Define double value for minimum");
                    String str3 = user.next();
                    str3 = DoubleCheck(str3);
                    if (str3.equals("f")){
                        do {
                            System.out.println("Input validation works. Rejected invalid input for minimum double value\nInput valid data now:");
                            str3 = user.next();
                            str3 = DoubleCheck(str3);
                            error = str3.equals("f");
                        }while (error);
                    }
                    System.out.println("Define double value for maximum");
                    String str4 = user.next();
                    str4 = DoubleCheck(str4);
                    if (str4.equals("f")){
                        do {
                            System.out.println("Input validation works. Rejected invalid input for maximum double value\nInput valid data now:");
                            str4 = user.next();
                            str4 = DoubleCheck(str4);
                            error = str4.equals("f");
                        }while (error);
                    }
                    num3 = Double.parseDouble(str3);
                    num4 = Double.parseDouble(str4);

                    if (num3 < num4) {
                        System.out.println("Running getDouble() with range of [" + num3 + "]-[" + num4 + "]\n");
                        double case5 = testUser.getDouble(num3, num4);
                        System.out.println("getDouble(double mint, double gum) returned: '"+case5+"'");
                    } else if (num3 > num4){
                        System.out.println("Minimum provided was greater than Maximum provided");
                        System.out.println("Running getDouble() with range of [" + num4 + "]-[" + num3 + "]\n");
                        double case5 = testUser.getDouble(num4, num3);
                        System.out.println("getDouble(double mint, double gum) returned: '"+case5+"'");
                    } else if (num3 == num4){
                        System.out.println("Values provided for minimum and maximum were the same.");
                        num3 = 0.0;
                        num4 = 1.0;
                        System.out.println("Running getDouble() with range of [" + num3 + "]-[" + num4 + "]\n");
                        double case5 = testUser.getDouble(num3, num4);
                        System.out.println("getDouble(double mint, double gum) returned: '"+case5+"'");
                    }
                    break;



                default:
                    if (quitTimer <= 0) {
                        System.out.println("Aborting program.");
                        quit = true;
                        break;
                    } else {
                        System.out.println("No.");
                        quitTimer--;
                    }

            }
        }while (!quit);
    }


    public static  String  IntCheck(String test){
        try{
            Integer.parseInt(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "f";
        }
        return test;
    }

    public static String DoubleCheck(String test){
        try{
            Double.parseDouble(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "f";
        }
        return test;
    }

}
