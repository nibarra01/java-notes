package CodeboundGymProject;

import java.util.ArrayList;
import java.util.LinkedList;

public class CodeboundClubApp {
    public static void main(String[] args) {
        boolean quit = false;
        String add;
        LinkedList<Member> members = new LinkedList<>();


        do {
            members = MemberFileHandler.readFile();
            int choice = MembershipManagement.getChoice();

            switch (choice) {
                case 1:
                    add = MembershipManagement.addMembers(members);
                    MemberFileHandler.appendFile(add, members);
                    break;
                case 2:
                    members = MembershipManagement.removeMember(members);
                    MemberFileHandler.overwriteFile(members);
                    break;
                case 3:
                    MembershipManagement.printMemberInfo(members);
                    break;
                case 0:
                    System.out.println("Exiting program.");
                    quit = true;
            }
        }while (!quit);
    }
}
