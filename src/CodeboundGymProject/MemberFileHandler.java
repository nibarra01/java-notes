package CodeboundGymProject;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;

public class MemberFileHandler {

    public static LinkedList<Member> readFile(){
        String line;
        int marker;
        char a;
        int b,e;
        double d;
        String c;
        LinkedList<Member> memberList = new LinkedList<>();
        try(BufferedReader br = new BufferedReader(new FileReader("src/CodeboundGymProject/codeboundGymData/members.csv"))){
            line = br.readLine();
            while (line != null) {

                marker = line.indexOf("§");
                a = line.charAt(0);
                line = line.substring(marker + 1);
                marker = line.indexOf("§");
                b = Integer.parseInt(line.substring(0, marker));
                line = line.substring(marker + 1);
                marker = line.indexOf("§");
                c = line.substring(0, marker);
                line = line.substring(marker + 1);
                marker = line.indexOf("§");
                d = Double.parseDouble(line.substring(0, marker));
                line = line.substring(marker + 1);
                e = Integer.parseInt(line);

                if (a == 'S') {
                    memberList.add(new SingleClubMember(a, b, c, d, e));
                } else if (a == 'M') {
                    memberList.add(new MultiClubMember(a, b, c, d, e));
                }

//                System.out.println(a + b + c + d + e);

                line = br.readLine();
            }
//            System.out.println("memberList: " + memberList);
        }
        catch (ArrayIndexOutOfBoundsException bad){
            System.out.println("Error " + bad.getMessage() + "Caused by " + bad.getCause());
        }
        catch (IOException bad){
            System.out.println("╔═══════════════════════╗");
            System.out.println("║ !!!   !!!   !!!   !!! ║\n║ members.csv not found ║\n║ !!!   !!!   !!!   !!! ║");
            System.out.println("╚═══════════════════════╝");
        }
        if (memberList.isEmpty()){
            System.out.println("╔══════════════════════════╗");
            System.out.println("║ !!   !!   !!   !!   !!   ║\n║ THE MEMBER LIST IS EMPTY ║\n║ !!   !!   !!   !!   !!   ║");
            System.out.println("╚══════════════════════════╝");
        }
        return memberList;

    }



    public static void appendFile(String input, LinkedList<Member> memberList) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/CodeboundGymProject/codeboundGymData/members.csv"))) {


            for (Member member : memberList) {
//                System.out.println("member: " + member.toString());
                writer.write(member.toString() + "\n");
            }
//            writer.write(input);

            }
        catch (IOException bad){
            System.out.println(bad.getMessage());
        }

        }


        public static void overwriteFile(LinkedList<Member> members){
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("src/CodeboundGymProject/codeboundGymData/members.csv"))) {

                for (Member member : members) {
                    writer.write(member.toString() + "\n");
                }

            }
            catch (IOException bad){
                System.out.println(bad.getMessage());
            }

        }


}

