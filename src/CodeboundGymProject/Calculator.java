package CodeboundGymProject;

public interface Calculator {
    static double calculateFees(int clubID){
        double fee;
        switch (clubID){
            case 1:
                return 900.00;
            case 2:
                return 950.00;
            case 3:
                return 1000.00;
            case 4:
                return 1200.00;
            default:
                return 0;
        }
    }

}
