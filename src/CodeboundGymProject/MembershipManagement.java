package CodeboundGymProject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MembershipManagement {
    private static int getIntInput(int min, int max){
        Scanner in = new Scanner(System.in);
        String choice1;
        int choice2;
        boolean valid = false;

        choice1 = in.next();

        try{
            choice2 = Integer.parseInt(choice1);
        } catch (Exception bad) {
            System.out.println("Input was invalid.");
            choice2 = -5;
        }



        if (choice2 <= min && choice2 >= max) {
            do {
            System.out.println("Accepted range is between " + min + " and " + max);
            choice1 = in.next();
            try{
                choice2 = Integer.parseInt(choice1);
            } catch (Exception bad) {
                System.out.println("Input was invalid.");
                choice2 = -5;
            }
//                choice2 = Integer.parseInt(choice1);
                valid = choice2 >= min && choice2 <= max;
            } while (!valid);
        }

                return choice2;
    }

    private static void printClubOptions(){
        System.out.println("┌───────────────────────┐");
        System.out.println("│ [1] Club Alpha\t\t│" +
                "\n│ [2] Club Bravo\t\t│" +
                "\n│ [3] Club Charlie\t\t│" +
                "\n│ [4] Multi-Club Member │");
        System.out.println("└───────────────────────┘");
    }

    public static int getChoice(){
        int choice;
        System.out.println("┌─────────────────────────────────────┐");
        System.out.println("│ WELCOME TO CODEBOUND FITNESS CENTER │" +
                "\n├─────────────────────────────────────┤" +
                "\n│ Please select an option             │" +
                "\n│ [1] Add Member                      │" +
                "\n│ [2] Remove Member                   │" +
                "\n│ [3] Display Member Information      │" +
                "\n│ [0] Quit                            │");
        System.out.println("└─────────────────────────────────────┘");

        choice = getIntInput(0,3);
        return choice;
    }

    public static String addMembers(LinkedList<Member> memberList){
        String name;
        int club;
        double fees;
        int memberID=0;
        Member mbr;
        Scanner in = new Scanner(System.in);


        System.out.println("Enter the name for the new member:");
        name = in.nextLine();
        System.out.println();

        printClubOptions();
        System.out.println("Select membership for " + name);
        club = getIntInput(1,4);

        if(memberList.size()>0){
            for(Member x : memberList){
                if (x.getMemberID() > memberID) {
                    memberID = x.getMemberID();
                }
            }
        }

        memberID += 1;

        fees = Calculator.calculateFees(club);

        if (club < 4){
            mbr = new SingleClubMember('S', memberID, name, fees, club);
            memberList.add(mbr);
            System.out.println("Single Club Member " + name + " added.");
        }else {
            mbr = new MultiClubMember('M', memberID,name,fees, 100);
            memberList.add(mbr);
            System.out.println("Multi-Club Member " + name + " added.");
        }
//        System.out.println(mbr.toString());
//        System.out.println(memberList);
        return mbr.toString();
    }

    public static LinkedList<Member> removeMember(LinkedList<Member> memberList){
        int del;
        int delSuccess = 0;
        if (!memberList.isEmpty()){
            int max=0;
            for(Member x : memberList){
//                System.out.println(x);
                if (x.getMemberID() > max) {
                    max = x.getMemberID();
                }
                System.out.println(x.getMemberID() + " " + max);
            }

            int initialLength = memberList.size();


            System.out.println("Enter the memberID of the member to be removed:");
            del = getIntInput(1, max);
            System.out.println(del);
//            for (Member delete : memberList){
//                System.out.println(delete);
//                if (delete.getMemberID() == del){
//                    System.out.println(delete.getMemberID() + " " + del);
//                    // found solution after googling Concurrent Modification Exception
////                    mevermind this deleted the entire file
////                    memberList.removeIf((deleteThis -> deleteThis.getMemberID() == del));
//
//                    delSuccess++;
//                }
//            }

            memberList.removeIf(deleteThis -> deleteThis.getMemberID() == del);



            switch (delSuccess){
                case 0:
                    System.out.println("Member ID not found");
                    return memberList;
                case 1:
                    System.out.println("Member Removed");
                    return memberList;
                default:
                    System.out.println("Multiple deletions detected. Verify data integrity");
                    return memberList;
            }

        }else {
            System.out.println("╔═══════════════════════════════════╗");
            System.out.println("║ !!!   !!!   !!!   !!!   !!!   !!! ║");
            System.out.println("║ UNABLE TO DELETE MEMBERS          ║");
            System.out.println("║ THE MEMBER LIST IS EMPTY/MISSING  ║");
            System.out.println("║ !!!   !!!   !!!   !!!   !!!   !!! ║");
            System.out.println("╚═══════════════════════════════════╝");
            return memberList;
        }
    }

    public static void printMemberInfo(LinkedList<Member> memberList){
        Scanner in = new Scanner(System.in);
        int ID;


        if (!memberList.isEmpty()){

            int max =0;

            for (Member scan : memberList){
                if (scan.getMemberID() > max){
                    max = scan.getMemberID();
                }
                System.out.println("│ Name: " + scan.getName() + "\t│ ID:\t" + scan.getMemberID() + "\t│" + scan.getName().length()/4);
            }

            System.out.println("Enter MemberID to display information:");
            ID = getIntInput(1,max);

            for (Member mbr : memberList){
                if (mbr.getMemberID() == ID){
                    switch (mbr.getMemberType()){
                        case 'S':
                            SingleClubMember Smbr = (SingleClubMember)mbr; //this actually works?
                            System.out.print("Information for member " + mbr.getName() + "\n" +
                                    "Member Type: Single-club\n" +
                                    "Member ID: " + mbr.getMemberID() + "\n" +
                                    "Membership Fees: " + mbr.getFees() + "\n"
                            );
                            switch (Smbr.getClub()){
                                case 1:
                                    System.out.print("Club: Alpha\n");
                                    break;
                                case 2:
                                    System.out.print("Club: Bravo\n");
                                    break;
                                case 3:
                                    System.out.print("Club: Charlie\n");
                            }
                            break;
                        case 'M':
                            MultiClubMember Mmbr = (MultiClubMember)mbr;
                            System.out.print("Information for member " + mbr.getName() + "\n" +
                                    "Member Type: Multi-club\n" +
                                    "Member ID: " + mbr.getMemberID() + "\n" +
                                    "Membership Fees: " + mbr.getFees() + "\n" +
                                    "Membership Points: " + Mmbr.getMembershipPoints() + "\n");
                            break;
                        default:
                            System.out.println("Something happened");
                            break;
                    }
                }
            }



        }else {
            System.out.println("╔══════════════════════════════════╗");
            System.out.println("║ !!   !!   !!   !!   !!   !!   !! ║");
            System.out.println("║ THE MEMBER LIST IS EMPTY/MISSING ║");
            System.out.println("║ NO MEMBER DATA TO PRINT          ║");
            System.out.println("║ !!   !!   !!   !!   !!   !!   !! ║");
            System.out.println("╚══════════════════════════════════╝");
        }
        System.out.println();
    }
}
