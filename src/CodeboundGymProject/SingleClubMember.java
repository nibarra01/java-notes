package CodeboundGymProject;

public class SingleClubMember extends  Member{
    private int club;

    public int getClub() {
        return club;
    }

    public void setClub(int club) {
        this.club = club;
    }

    public SingleClubMember(char memberType, int memberID, String name, double fees) {
        super(memberType, memberID, name, fees);

    }

    public SingleClubMember(char memberType, int memberID, String name, double fees, int club) {
        super(memberType, memberID, name, fees);
        this.club = club;
    }

    @Override
    public String toString(){
        return this.getMemberType() + "§" + this.getMemberID() + "§" + this.getName() + "§" + this.getFees() + "§" + this.club;
//        return this.getMemberType() + "," + this.getMemberID() + "," + this.getName() + "," + this.getFees() + "," + this.club;
    }
}
