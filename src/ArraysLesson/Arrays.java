package ArraysLesson;

public class Arrays {
    /*
    In Java arrays are a different kind of object that contain zero or more items called elements
    Array elements can be any valid type but all elements must be of the same type

    Syntax:
    type[] nameOfTheVariable;
     */

    double[] prices;

    /*
    In Java arrays have a fixed length, this is accessed by using the .length property
    They have to be defined when they are created.

    The size of the array can either be a literal, a constant or a variable
    Arrays are zero index
     */

    public static void main(String[] args) {
//        example
        String[] developers = new String[5];
        developers[0] = "Josh";
        developers[1] = "Nick";
        developers[2] = "Jose";
        developers[3] = "Stephen";
        developers[4] = "Karen";

//        System.out.println(developers[3]); //Stephen
//        System.out.println(developers.length); //5
//
//        Assigning a variable we created to a new array where the size is determined by a constant defined before
//        int numOfBugs = (int) Math.floor(Math.random() * 100);
//        Bug[] myCode = new Bug[numOfBugs];

        int[] numbers = new int[3];
        numbers[0] = 11;
        numbers[1] = 12;
        numbers[2] = 13;

//        ArrayIndexOutOfBoundsException
//        numbers[3] = 14;
//        System.out.println(numbers[3]);


//        iterating
//        like js, we can use the length property of any array in combo with looping construct object(s) to iterate it

        String[] lang = {"html", "css", "javascript","jquery","angular","java"};
        for (int i = 0; i < lang.length; i++){
//            System.out.println(lang[i]);
        }

        System.out.println();

//        alter / "Enhanced for" )java's forEach
        String[] gameConsoles ={"PC", "DS", "3DS", "PS2", "PS3", "PS4", "Switch", "N64", "SNES", "Gamecube", "Xbox", "Wii"};
        for (String gc : gameConsoles){
//            System.out.println(gc);
        }

        System.out.println();


//        two-dimentional array
//        "matrix" / grid-like array

        int[][] myMatrix = {
                {1,2,2},
                {4,5,6},
                {7,8,9}

        };

//        access the first element in the second row

//        System.out.println(myMatrix[2][1]); // 8
//        System.out.println(myMatrix[0][0]); // 1
//        System.out.println(myMatrix[1][0]); // 4
//        System.out.println(myMatrix[2][2]); // 9
//

        // iterate through the matrix using a nested loop

        for(int[] row: myMatrix){ // first loop targeting the first array (row
            System.out.println("+---+---+---+");
            System.out.print("| ");
            for (int n : row){ // second loop targeting the second array(individual number)
                System.out.print(n + " | ");
            }
            System.out.println();
        }
        System.out.println("+---+---+---+");


    }
}
