public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("Welcome to JAVA");

//        single line comment
        /*
        multi
        line
        comment
        here
         */

        // DATA TYPES
        /*
        byte - very short integers from -128 to 127
        short - short integers from -32,768 - 32,767
        int - integers from -2,147,483,648 to 2,147,283,647
        long - integers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
        float - fractional numbers 6 to 7 decimal places
        double - fractional numbers 15 decimal digits
        boolean - stores true or false values
        char - stores a single character/letter or ASCII values
         */

        // STRINGS
//        "String / long sentences";
//        'C';

        System.out.println("Escape characters:");
        System.out.println("First \\"); // \
        System.out.println("Second \n"); // new line
        System.out.println("Third \t"); // tab character(s)

        // VARIABLES
        // all variables in Java must be declared before they are used
        // Syntax:
        // dataType nameOfVariable;
        byte age = 12;
        short mySHort = -32768;
        int myInteger;
        myInteger = 18;
        System.out.println(myInteger);

        boolean userLoggedIn = false;
        System.out.println(userLoggedIn);

        int num1;
        int num2;
        num1 = 12;
        num2 = 13;
        int sum = num1 + num2;
        System.out.println(sum);

        // CASTING
        /*
        turning a value of one type into another.
        Two types of casting: implicit / explicit casting

        IMPLICIT CASTING
        - involves assigning a value of a less precise data type to a variable whose types is of a higher precision.
         */
        int myInteger2 = 900;
        long morePrecise = myInteger2;
        System.out.println(morePrecise);

        //EXPLICIT CASTING
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);
    }
}
