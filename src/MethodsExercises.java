import java.util.InputMismatchException;
import java.util.Scanner;

public class MethodsExercises {
    public static void main(String[] args) {


        boolean n = false;
        do {
        Scanner mainScanner = new Scanner(System.in);
        System.out.println("Choose your destiny:\n" +
                "A. Arithmetic\n" +
                "B. Validate Input\n" +
                "C. Factorials\n" +
                "D. Average\n" +
                "Q. Quit ");
        String program = mainScanner.next().toUpperCase();


            switch (program) {
                case "A":
                    Arethmetic();
                    break;
                case "B":
                    ValidateInput();
                    break;
                case "C":
                    Factorials();
                    break;
                case "D":
                    Average();
                    break;
                case "Q":
                    System.out.println("Okay.");
                    n = false;
                    break;
                default:
                    System.out.println("No.\n\n");
                    n = true;
                    break;
            }
        }while (n);








    }
    //        ======================================================================================
    public static void Arethmetic(){
        System.out.println("Program begins");
        System.out.println("Select which mathyness to begin mathifying:");
        Scanner input = new Scanner(System.in);
        String  choice = "more effort than I thought to use char";

        boolean bool = false;
        int noCount = 0;
        while (bool == false) {
//            != != .equals()



            System.out.println("a. Addition\n" +
                    "b. Subtraction\n" +
                    "c. Multiplication\n" +
                    "d. Division only\n" +
                    "e. Modulus/Remainder only\n" +
                    "f. Division with Remainder\n" +
                    "q. Quit\n");
            choice = input.next().toLowerCase();


            switch (choice){
                case "q":
                    bool = true;
                    System.out.println("End of line.");
                    break;
                case "a":
                    System.out.println("Addition time");
                    System.out.println("input number 1");
                    float add1 = input.nextFloat();
                    System.out.println("input number 2");
                    float add2 = input.nextFloat();
                    float sum = Addition(add1, add2);
                    break;
                case "b":
                    System.out.println("Subtraction time");
                    System.out.println("input number 1");
                    float sub1 = input.nextFloat();
                    System.out.println("input number 2");
                    float sub2 = input.nextFloat();
                    float diff = Subtraction(sub1, sub2);
                    break;
                case "c":
                    System.out.println("Multiplication time");
                    System.out.println("input number 1");
                    float mul1 = input.nextFloat();
                    System.out.println("input number 2");
                    float mul2 = input.nextFloat();
                    float prod = Multiplication(mul1, mul2);
                    break;
                case "d":
                    System.out.println("Division time");
                    System.out.println("input number 1");
                    float div1 = input.nextFloat();
                    System.out.println("input number 2");
                    float div2 = input.nextFloat();
                    float divided = Division(div1, div2);
                    break;
                case "e":
                    System.out.println("% time");
                    System.out.println("input number 1");
                    float mod1 = input.nextFloat();
                    System.out.println("input number 2");
                    float mod2 = input.nextFloat();
                    float rem = Modulus(mod1, mod2);
                    break;
                case "f":
                    System.out.println("Dividing time");
                    System.out.println("input number 1");
                    float com1 = input.nextFloat();
                    System.out.println("input number 2");
                    float com2 = input.nextFloat();
                    String combo = Combo(com1, com2);
                    String splitCombo[] = combo.split(",");
//                    System.out.println(splitCombo);
                    float comboAns1 = Float.parseFloat(splitCombo[0]);
                    float comboAns2 = Float.parseFloat(splitCombo[1]);
                    break;
                default:
                    noCount++;
                    if (noCount >5){
                        System.out.println("NO! STOP BEING WRONG!\n");
                    } else{
                        System.out.println("No.\n");
                    }
            }



        }
    }

    public static float Addition(float a, float b){
        float c = a + b;
        if (a % 1 == 0 && b % 1 == 0){
            System.out.printf("%.0f and %.0f are the entered values\nThe sum is: %.0f\n",a,b,c);
        } else {
            System.out.printf("%s and %s are the entered values\nThe sum is: %s\n",a,b,c);
        }
        System.out.println();
        return c;
    }

    public static float Subtraction(float a, float b){
        float c = a - b;
        if (a % 1 == 0 && b % 1 == 0){
            System.out.printf("%.0f and %.0f are the entered values\nThe difference is: %.0f\n",a,b,c);
        } else {
            System.out.printf("%s and %s are the entered values\nThe difference is: %s\n",a,b,c);
        }
        System.out.println();
        return c;
    }

    public static float Multiplication(float a, float b){
        float c = a * b;
        if (a % 1 == 0 && b % 1 == 0){
            System.out.printf("%.0f and %.0f are the entered values\nThe product is: %.0f\n",a,b,c);
        } else {
            System.out.printf("%s and %s are the entered values\nThe product is: %s\n",a,b,c);
        }
        System.out.println();
        return c;
    }

    public static float Division(float a, float b){
        float c = a / b;
        if (a % 1 == 0 && b % 1 == 0){
            System.out.printf("%.0f and %.0f are the entered values\nThe whatever-the-word-for-the-result-of-dividing-is is: %.0f\n",a,b,c);
        } else {
            System.out.printf("%s and %s are the entered values\nThe whatever-the-word-for-the-result-of-dividing-is is: %s\n",a,b,c);
        }
        System.out.println();
        return c;
    }

    public static float Modulus(float a, float b){
        float c = a % b;
        if (a % 1 == 0 && b % 1 == 0){
            System.out.printf("%.0f and %.0f are the entered values\nThe remainder is: %.0f\n",a,b,c);
        } else {
            System.out.printf("%s and %s are the entered values\nThe remainder is: %s\n",a,b,c);
        }
        System.out.println();
        return c;
    }
    public static String Combo(float a, float b){
        float c = a / b;
        float d = a % b;

        if (a % 1 == 0 && b % 1 == 0) {
            System.out.printf("%.0f and %.0f are the entered values\n", a, b);
            System.out.printf("%.0f divided by %.0f is %.0f with a remainder of %.0f\n", a, b, c, d);
        }else {
            System.out.printf("%s and %s are the entered values\n",a,b);
            System.out.printf("%s divided by %s is %s with a remainder of %s\n",a,b,c,d);
        }
        String e = c + "," + d;
        System.out.println();
        return e;
    }


    //----------------------------------------------------------------------------------------------------

    public static void ValidateInput(){
        System.out.println("Program begins");
        Scanner scans = new Scanner(System.in);
        System.out.println("\nEnter a number between 1 and 10");
        if (scans.hasNextInt()){
            int number = scans.nextInt();
            if (numberChecker(number)){
                boolean runAgain = false;
                do {
                    runAgain = false;
                    System.out.println(number + " is between 1 and 10");
                    System.out.println("\nProgram completes. \nRun again (Y/N)?");
                    String passResponse = scans.next().toUpperCase();
                    switch (passResponse){
                        case "Y":
                            System.out.println("Restarting...");
                            System.out.println("\n\n\n");
                            ValidateInput();
                            break;
                        case "N":
                            System.out.println("End of line.");
                            break;
                        default:
                            System.out.println("Invalid response\n\n");
                            runAgain = true;
                            break;

                    }
                } while(runAgain);

            }else {
                boolean repeat = false;
                do {
                    repeat = false;
                System.out.println("\n Quit or Retry?\nQ. Quit\nR. Retry");
                String failResponse = scans.next().toUpperCase();
                switch (failResponse) {
                    case "Q":
                        System.out.println("\nEnd of line.");
                        break;
                    case "R":
                        System.out.println("\nRestarting...");
                        System.out.println("\n\n\n");
                        ValidateInput();
                        break;
                    default:
                        System.out.println("\nNo.\n\n");
                        repeat = true;
                        break;
                }

                } while (repeat);
            }
        } else {
            System.out.println("NO");
            ValidateInput();
        }

    }


    public static boolean numberChecker(int x){
        if (x <= 0 || x >= 11){
            System.out.println("Invalid");
            if (x <= 0){
                System.out.println(x + " is under the valid range");
            } else {
                System.out.println(x + " is over the valid range");
            }
            return false;
        } else {
            System.out.println("Valid");
            return true;
        }
    }

//    ****************************************************************************************

    public static void Factorials(){
        System.out.println("Program begins.");
        System.out.println("Enter a number for factorial (valid range 1-10):");
        Scanner scn = new Scanner(System.in);
        if (scn.hasNextInt()){
            long fact = scn.nextLong();
            FactorialsSubmethod(fact);
        } else {
            System.out.println("\nNo.\n");
            Factorials();
        }


        }

        public static void FactorialsSubmethod(long i){
            Scanner subscans = new Scanner(System.in);
            boolean restart = false;

        if (i <= 0){
            System.out.println(i + " is under the accepted range.\n" +
                    "[Q]uit or [R]etry?");
            do{
                String select = subscans.next().toUpperCase();
                switch (select){
                    case "R":
                        System.out.println("Program restarts.\n");
                        Factorials();
                        break;
                    case "Q":
                        System.out.println("Program ends.");
                        restart = false;
                        break;
                    default:
                        System.out.println("invalid input.\n");
                        restart = true;
                        break;
                }
            } while (restart);
        } else if (i >= 11){
            System.out.println(i + " is over the accepted range.\n" +
                    "[Q]uit or [R]etry?");
            do{
                String select = subscans.next().toUpperCase();
                switch (select){
                    case "R":
                        System.out.println("Program restarts.\n");
                        Factorials();
                        break;
                    case "Q":
                        System.out.println("Program ends.");
                        restart = false;
                        break;
                    default:
                        System.out.println("invalid input.\n");
                        restart = true;
                        break;
                }
            } while (restart);
        } else {

            long ans = 1;

            System.out.println("Displaying factorial of "+ i);
            for (long x = 1; x <= i; x++){
                ans *= x;
                if (x == 1){
                    System.out.print(i+"! = " + x);
                } else if (x == i) {
                    System.out.print(" x " + x + " = " + ans);
                } else {
                        System.out.print(" x " + x);
                    }
                }
            System.out.println("\n\nProgram completes.\n Run again(Y/N)?");
            do{
                String select = subscans.next().toUpperCase();
                switch (select){
                    case "Y":
                        System.out.println("Program restarts.\n");
                        Factorials();
                        break;
                    case "N":
                        System.out.println("Program ends.");
                        restart = false;
                        break;
                    default:
                        System.out.println("invalid input.\n");
                        restart = true;
                        break;
                }
            } while (restart);
            }

        }

//        _________________________________________________________________________________________________________

    public static void Average(){
        System.out.println("Program begins.");
        Scanner avgScan =  new Scanner(System.in);
//        boolean invalid1 = false;
//        boolean invalid2 = false;
//        boolean invalid3 = false;

        System.out.println("\nCompute the average of three numbers\nInput the first number:");

//        infinte loop when if/else
//        found similar problem on stackoverflow, changed NumberFormatException to InputMismatchException displayed in error
//        these need to be in their own method?

        String num1s = Avg();

        System.out.println("\nInput the second number:");
        String num2s = Avg();

        System.out.println("\nInput the third number:");
        String num3s = Avg();

        System.out.println("\nConfirming numbers: ");
//        System.out.println(num1S);
//        System.out.println(num2S);
//        System.out.println(num3s);


        if (num1s.equals("failed")){
            System.out.println("Number 1 is invalid");
        }
        if (num2s.equals("failed")){
            System.out.println("Number 2 is invalid");
        }
        if (num3s.equals("failed")){
            System.out.println("Number 3 is invalid");
        }
        while (num1s.equals("failed")){
            System.out.println("Re-enter the first number:");
            num1s = Avg();
        }
        while (num2s.equals("failed")){
            System.out.println("Re-enter the second number:");
            num2s = Avg();
        }
        while (num3s.equals("failed")){
            System.out.println("Re-enter the third number:");
            num3s = Avg();
        }

        float num1 = Float.parseFloat(num1s);
        float num2 = Float.parseFloat(num2s);
        float num3 = Float.parseFloat(num3s);

        boolean showDecimals = true;
//        System.out.println(num1 % 1);
//        System.out.println(num2 % 1);
//        System.out.println(num3 % 1);
        if (num1 % 1 == 0.0 && num2 % 1 == 0.0 && num3 % 1 == 0.0){
            showDecimals = false;
        }

        float total = num1 + num2 + num3;
        float answer = total / 3;
//        System.out.println(showDecimals);

        if (!showDecimals){
            System.out.printf("%.0f, %.0f and %.0f are the entered values\n", num1, num2, num3);
            System.out.println(answer +" is the average");

        } else {
            System.out.println(num1 + ", " + num2 + " and " + num3 + " are the entered values");
            System.out.println(answer + " is the average");
        }


        }

        public static String Avg(){
        Scanner sc = new Scanner(System.in);
        String num = sc.next();
            try{
                Float.parseFloat(num);
            }
            catch (InputMismatchException | NumberFormatException e){
                return "failed";
            }
            return num;
        }
    }


//}
