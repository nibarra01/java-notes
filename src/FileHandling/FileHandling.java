package FileHandling;

import java.io.*;

/*
Main Focus: File, BufferedReader, FileReader, BufferedWriter,
and FileWriter from java.io package.

how to import?
import java.io.*;











Alternate java libraries:
Path Interface, Paths class,
Files class from the java.io and java.nio packages

File class
- represents the files and directory pathnames in an abstract manner.
- used for creating files and directories, file searching and
file deletion

BufferedReader class
- reads text from a character-input stream,
- buffering characters so as to provide for the efficient
reading of characters, arrays, and lines
- NOTE: buffer size may be specified or the default size may be used.

BufferedWriter class
- Creates a buffered character-output stream that uses a default-sized
output buffer
- it inherits the Writer Class

FileWriter class
- used to write character-oriented data to a file
- You don't need to convert string into byte array
    - provides method to write string(s) directly

FileReader class
- used to read data from the file
- return data in byte format


Paths:
- absolute paths: specified from the filesystem root
- relative paths: relative to the CURRENT WORKING DIRECTORY

EX
/
    Users/
        codebound/
            Documents/
                important-stuff.txt
            IdeaProjects/
                charlie-java-notes/  <-- current working directory
                    src/
                        FileTastic.java
                    data/
                        data.txt

relative paths
- data/data.txt
- ./data/data.txt

absolute paths
- /Users/codebound/IdeaProjects/charlie-java-notes/data/data.txt

 */

public class FileHandling {
    public static void main(String[] args) {


        //Reading if there's a text file with a name
        //use the FileReader class with a BufferedReader class

//    create a string variable
        String line;

//    create a BufferedReader object
        BufferedReader bufferedReader = null;

//    try-catch-finally
        try {
            bufferedReader = new BufferedReader(new FileReader("src/FileHandling/MovieQuotes/hamlet.txt"));
            line = bufferedReader.readLine();

            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try{
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }
            catch ( IOException e){
                System.out.println(e.getMessage());
            }
        }

//        writing to a text file
//        use FileWriter with a BufferedWriter class

//        create a string variable
        String text = "I'll be back";

//        try(BufferedWriter writer = new BufferedWriter(new FileWriter("src/FileHandling/MovieQuotes/terminator.txt"))){
//            writer.write(text);
//            writer.newLine();
//        }
//        catch (IOException e){
//            e.getCause();
//        }

        //overwriting a text file/file's text

//        String overwriteText = "Listen to many, speak to a few";
//        try (BufferedWriter rewrite = new BufferedWriter(new FileWriter("src/FileHandling/MovieQuotes/hamlet.txt"))) {
//            rewrite.write(overwriteText);
//            rewrite.newLine();
//        }catch (IOException e){
//            System.out.println(e.getMessage());
//        }

//        renaming a file

////        targeting the file we want to rename
//        File oldFileName = new File("src/FileHandling/MovieQuotes/terminator.txt");
//
////        creating the name we want
//        File newFileName = new File("src/FileHandling/MovieQuotes/terminator2.txt");
//
//        oldFileName.renameTo(newFileName);
//

//        deleting a text file
        File newFileName = new File("src/FileHandling/movieQuotes/terminator2.txt");
        newFileName.delete();
//        cli: 'rm -rf fileNMW

    }
}
