public class CarMain {
    public static void main(String[] args) {
        Car c1 = new Car();
        c1.maker = "Hyundai";
        c1.model = "forgot";
        c1.year = 1996;
        c1.setName("0th car, 1.5 days");

        Car c2 = new Car();
        c2.maker = "Nissan";
        c2.model = "Sentra";
        c2.year = 2002;
        c2.setName("First car");

        Car c3 = new Car();
        c3.maker = "Nissan";
        c3.model = "Sentra";
        c3.year = 2015;
        c3.setName("Second car");


    }
}
