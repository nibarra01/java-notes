public class StringsLesson {
    public static void main(String[] args) {
//        strings
        String message = "hello world";
        System.out.println(message);

        String anotherMessage = "Another message assigned";

        // Concatenation
        System.out.println(message + " " + anotherMessage);

        //format
        System.out.format("%s %s", message, anotherMessage);

//        string comparison  methods
//        .equals(), .equalsIgnoreCase(), .startsWith(), .endsWith()
        String hero = "Batman";
//        if (hero == "Batman") { //no
//            System.out.println("Adam West);
//        }

        if (hero.equals("Batman")){ //yes
            System.out.println("Adam West");
        }

        String input = "Charlie Rocks!";
//        System.out.println(input.equals("charlie rocks!"));
//        System.out.println(input.equals("Charlie rocks!"));
//        System.out.println(input.equals("CHARLIE ROCKS!"));
//        System.out.println(input.equals("Charlie Rocks"));

//        System.out.println(input.equalsIgnoreCase("charlie rocks"));
//        System.out.println(input.equalsIgnoreCase("charlie rocks!   "));
//        System.out.println(input.equalsIgnoreCase("CHARLIE ROCKS"));
//        System.out.println(input.equalsIgnoreCase("CHARLIE ROCKS!"));

//        System.out.println(input.startsWith("c"));
//        System.out.println(input.startsWith("C"));
//        System.out.println(input.endsWith("s"));
//        System.out.println(input.endsWith("!"));

        /*
        String manipulation methods
        char charAt(int index);
        charAt() - returns character at hte specified index of the string
        indexOf() - returns the index of the first occurence of a certain substring
        returns -1 if the substring is not found

        lastIndexOf() - like indexOf() but start the search from the end of the string

        length() - returns the length of the string

        replace() - returns a copy of the string that has oldChar replaced with newChar
        String myStr = "CodeBound";
        System.out.println(myStr.replace('B','Z')); // CodeZound
        System.out.println(myStr.replace('B','b')); // Codebound

        substring() - returns a new substring that starts at a specified index and (optionally) ends at the specified index

       String name ="Charlie";
      System.out.println(name.substring(1); // harlie

      toLowerCase() / toUpperCase()

      trim() - returns a copy of the string without leading or trailing whitespace
         */
    }
}
